# Deep Learning Master Tool
## Environment
Require: python: version 3 or greater
## 1. Install requirement
```pip install -r requirement.txt```

## 2. Run project
```Window: start_tool.bat```

```Linux: sh start_tool.sh```

## 3. Access tool
localhost:8000
