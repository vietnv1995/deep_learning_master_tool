import pandas as pd
from configloader import config
import os

def read_config():
    params_sort = config.get("result.params", ["t_acc", "t_auc"])
    result_path = config.get("result.path", "./results")
    return result_path, params_sort

if __name__ == "__main__":
    result_path, params_sort = read_config()
    files = os.listdir(result_path)
    dfs = []
    for file in files:
        df = pd.read_csv(os.path.join(result_path, file))
        df["file_name"] = [file]
        dfs.append(df)
    df_concat = pd.concat(dfs)
    print(df_concat.head())
    df_concat.sort_values(params_sort, ascending=[False for i in range(len(params_sort))], inplace=True)
    df_concat.to_csv("concat_result.csv", index=False)
    print(df_concat.head(10))