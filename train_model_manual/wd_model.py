import pandas as pd
import tensorflow as tf
import shutil
import time
from sklearn import metrics
import math
import os, itertools
import json
import sys
from configloader import config
from notify_client.slack_nofity import SlackNotifier
from bk_logger import logger
import random
from sklearn.utils import shuffle
from pandas.api.types import is_numeric_dtype, is_categorical_dtype
import traceback
from core.feature_engineering.feature_enginnering import FeatureEngineer

ROUND = 4
BASE_DIR_RESULT = os.path.join(os.getcwd(), "results")
BASE_DIR_CONFIG = os.path.join(os.getcwd(), "configs")
BASE_DIR_MODEL = os.path.join(os.getcwd(), "models")
BASE_DIR_KFOLD = os.path.join(os.getcwd(), "kfold_results")

def create_folder(path):
    if not os.path.exists(path):
        os.makedirs(path)

create_folder(BASE_DIR_RESULT)
create_folder(BASE_DIR_CONFIG)
create_folder(BASE_DIR_MODEL)
create_folder(BASE_DIR_KFOLD)

def read_config(path_to_column_config, path_to_model_config):
    with open(path_to_column_config, "r") as f:
        column_config = json.load(f)
    with open(path_to_model_config, "r") as f:
        model_config = json.load(f)
    column_config_format = make_config_column(column_config)
    all_config = {"column_config": column_config_format, "model_config": model_config}
    return all_config

def make_config_column(column_config):
    result = column_config.copy()
    ignore_columns = set(column_config.get("ignore_cols"))
    feature_cols = column_config.get("feature_cols")
    onehot_cols = []
    hash_cols = {}
    emb_cols = {}
    for col, value in feature_cols.items():
        if col in ignore_columns:
            continue
        tmp = value.split("-")
        if tmp[0] == "one_hot":
            onehot_cols.append(col)
        else:
            hash_cols[col] = int(tmp[0])
        if len(tmp) == 2:
            emb_cols[col] = int(tmp[1])
    result.pop("feature_cols")
    result["onehot_cols"] = onehot_cols
    result["hash_cols"] = hash_cols
    result["emb_cols"] = emb_cols
    return result

def parser_config(config):
    column_config = config.get("column_config").copy()
    model_config = config.get("model_config").copy()
    ignore_columns = column_config.get("ignore_cols")
    numberic_columns = column_config.get("num_cols")
    category_columns = column_config.get("cat_cols")
    cross_cols = column_config.get("cross_cols")
    if len(ignore_columns) > 0:
        numberic_columns = list(set(numberic_columns) - set(ignore_columns))
        category_columns = list(set(category_columns) - set(ignore_columns))

        new_cross_cols = {}
        new_wide_cols = []
        for name, v in cross_cols.items():
            names = name.split(" x ")
            names = [":".join(item.split(":")[0:-1]) for item in names]
            if len(set(names).intersection(set(ignore_columns))) == 0:
                new_cross_cols[name] = v
                new_wide_cols.append(name)
        column_config["num_cols"] = numberic_columns
        column_config["cat_cols"] = category_columns
        column_config["cross_cols"] = new_cross_cols
        column_config["wide_cols"] = new_wide_cols
        column_config["deep_cols"] = list(set(column_config["deep_cols"]) - set(ignore_columns))
        column_config["all_cols"] = list(set(column_config["all_cols"]) - set(ignore_columns))
        column_config["source_cols"] = list(set(column_config["source_cols"]) - set(ignore_columns))
    #parse model
    nn = model_config.get("nn")
    layers = [[int(item) for item in nn.get("layers").split("x")]]
    model_config["layers"] = layers
    dropouts = nn.get("dropouts") if nn.get("dropouts") != 0 else None
    model_config["dropouts"] = dropouts
    return column_config, model_config, numberic_columns, category_columns, ignore_columns


def make_dtype(category_columns):
    dtype = {}
    for category_column in category_columns:
        dtype[category_column] = "str"
    return dtype


def df_to_dataset(dataframe, num_epochs, shuffle=True, batch_size=32):
    dataframe = dataframe.copy()
    labels = dataframe.pop('TARGET')
    ds = tf.data.Dataset.from_tensor_slices((dict(dataframe), labels))
    if shuffle:
        ds = ds.shuffle(buffer_size=len(dataframe))
    ds = ds.repeat(num_epochs)
    ds = ds.batch(batch_size)
    return ds

def train_input_fn():
    return df_to_dataset(df_train, model_config.get("epochs"))

def train_input_fn_one_epoch():
    return df_to_dataset(df_train, 1, False, 32)

def eval_input_fn():
    return df_to_dataset(df_test, 1, False, 32)

def build_model_column(path_to_file, numberic_columns, config_columns):
    '''
    Notice that one hot column and hash column must use through indicator column
    :return:
    '''
    numberic_dict = {}
    for name in numberic_columns:
        numberic_dict[name] = tf.feature_column.numeric_column(name)
    print("Aaaaaaaaaa", numberic_dict.keys())
    onehot_dict = {}
    for name in config_columns['onehot_cols']:
        df = pd.read_csv(path_to_file, usecols=[name], dtype="str")
        onehot_dict[name] = tf.feature_column.categorical_column_with_vocabulary_list(
            name, df[name].unique().tolist()
        )
    hash_dict = {}
    for name, hash_size in config_columns['hash_cols'].items():
        hash_dict[name] = tf.feature_column.categorical_column_with_hash_bucket(
            name, hash_bucket_size=hash_size)
    emb_dict = {}
    for name, emb_size in config_columns['emb_cols'].items():
        if name in onehot_dict.keys():
            emb_dict[name] = tf.feature_column.embedding_column(
                onehot_dict.get(name), dimension=emb_size)
        else:
            emb_dict[name] = tf.feature_column.embedding_column(
                hash_dict.get(name), dimension=emb_size)
    cross_dict = {}
    # coss_cols have format col1_name:100 x col2_name:200.
    # We must split by " x " and remove number of uv after :
    for name, hash_size in config_columns['cross_cols'].items():
        names = name.split(" x ")
        names = [":".join(item.split(":")[0:-1]) for item in names]
        names = ["_".join(name.split(" ")) for name in names]
        print(name)
        print(hash_size)
        cross_dict[name] = tf.feature_column.crossed_column(names, hash_size)
    wide_columns = []
    deep_columns = []
    print("Wide: {}".format(config_columns['wide_cols']))
    print("Deep: {}".format(config_columns['deep_cols']))
    for name in config_columns['wide_cols']:
        if name in cross_dict.keys():
            wide_columns.append(cross_dict[name])
        elif name in numberic_dict.keys():
            wide_columns.append(numberic_dict.get(name))
        elif name in onehot_dict.keys():
            wide_columns.append(onehot_dict.get(name))
        else:
            wide_columns.append(hash_dict.get(name))
    for name in config_columns['deep_cols']:
        if name in cross_dict.keys():
            deep_columns.append(tf.feature_column.indicator_column(cross_dict[name]))
        elif name in numberic_dict.keys():
            deep_columns.append(numberic_dict.get(name))
        elif name in emb_dict.keys():
            deep_columns.append(emb_dict.get(name))
        elif name in onehot_dict.keys():
            deep_columns.append(tf.feature_column.indicator_column(onehot_dict.get(name)))
        else:
            deep_columns.append(tf.feature_column.indicator_column(hash_dict.get(name)))
    return wide_columns, deep_columns


def get_optimizer(config_model):
    learning_rate = config_model.get("learning_rate", 0.05)
    optimizer_name = config_model.get("optimizer")
    if optimizer_name == "ada":
        return tf.train.AdagradOptimizer(learning_rate=learning_rate)
    elif optimizer_name == "adam":
        return tf.train.AdamOptimizer(learning_rate=learning_rate)
    elif optimizer_name == "rms":
        return tf.train.RMSPropOptimizer(learning_rate=learning_rate)


def get_model(name, config_model, wide_columns, deep_columns, model_dir="./models"):
    tf.reset_default_graph()
    shutil.rmtree(model_dir, ignore_errors=True)
    run_config = tf.estimator.RunConfig().replace(
        session_config=tf.ConfigProto(device_count={'GPU': 0}))
    if name == 'wide':
        return tf.estimator.LinearClassifier(
            model_dir=model_dir,
            feature_columns=wide_columns,
            config=run_config,
            n_classes=9
        )
    elif name == 'deep':
        return tf.estimator.DNNClassifier(
            model_dir=model_dir,
            feature_columns=deep_columns,
            hidden_units=list(config_model.get('layers')[0]),
            dropout=config_model.get("dropouts", None),
            optimizer=get_optimizer(config_model),
            config=run_config,
            n_classes=9
        )
    elif name == 'wd':
        return tf.estimator.DNNLinearCombinedClassifier(
            model_dir=model_dir,
            linear_feature_columns=wide_columns,
            dnn_feature_columns=deep_columns,
            dnn_hidden_units=list(config_model.get('layers')[0]),
            dnn_optimizer=get_optimizer(config_model),
            dnn_dropout = config_model.get("dropouts", None),
            config=run_config,
            n_classes=9
        )

def calculate_metrics(list_metrics, model):
    res_train = {}
    res_test = {}

    y_train_real = df_train["TARGET"].values
    y_test_real = df_test["TARGET"].values

    test_pred_gens = model.predict(input_fn=eval_input_fn)
    results = model.evaluate(input_fn=eval_input_fn)
    res_train['loss'] = results.get('average_loss')
    y_test_pred = []
    y_test_classes = []
    for item in test_pred_gens:
        y_test_pred.append(item['probabilities'][1])
        y_test_classes.append(item['class_ids'][0])

    train_pred_gens = model.predict(input_fn=train_input_fn_one_epoch)
    y_train_pred = []
    y_train_classes = []
    for item in train_pred_gens:
        y_train_pred.append(item['probabilities'][1])
        y_train_classes.append(item['class_ids'][0])
    if df_train["TARGET"].nunique() == 2:
        test_tn, test_fp, test_fn, test_tp = metrics.confusion_matrix(y_test_real, y_test_classes).ravel()
        train_tn, train_fp, train_fn, train_tp = metrics.confusion_matrix(y_train_real, y_train_classes).ravel()
    for metric in list_metrics:
        if metric == 'auc':
            fpr, tpr, thresholds = metrics.roc_curve(y_test_real, y_test_pred)
            res_test['auc'] = round(metrics.auc(fpr, tpr), ROUND)

            fpr, tpr, thresholds = metrics.roc_curve(y_train_real, y_train_pred)
            res_train['auc'] = round(metrics.auc(fpr, tpr), ROUND)
        elif metric == 'acc':
            res_test['acc'] = round(metrics.accuracy_score(y_test_real, y_test_classes), ROUND)
            res_train['acc'] = round(metrics.accuracy_score(y_train_real, y_train_classes), ROUND)
        elif metric == 'confus':
            print(metrics.confusion_matrix(y_test_real, y_test_classes))
            res_test['confus'] = str([list(item) for item in metrics.confusion_matrix(y_test_real, y_test_classes)])
            res_train['confus'] = str([list(item) for item in metrics.confusion_matrix(y_train_real, y_train_classes)])
        elif metric == 'precision':
            res_test['precision'] = round(metrics.precision_score(y_test_real, y_test_classes), ROUND)
            res_train['precision'] = round(metrics.precision_score(y_train_real, y_train_classes), ROUND)
        elif metric == 'recall':
            res_test['recall'] = round(metrics.recall_score(y_test_real, y_test_classes), ROUND)
            res_train['recall'] = round(metrics.recall_score(y_train_real, y_train_classes), ROUND)
        elif metric == 'f1_score':
            res_test['f1'] = round(metrics.f1_score(y_test_real, y_test_classes), ROUND)
            res_train['f1'] = round(metrics.f1_score(y_train_real, y_train_classes), ROUND)
        elif metric == 'specificity':
            res_test['specificity'] = round(test_tn / (test_tn+test_fp), ROUND)
            res_test['percent_0'] = round((test_fp + test_tn) / (test_tn + test_fp + test_fn + test_tp)*100, ROUND)
            res_test['percent_1'] = 100-res_test['percent_0']


            res_train['specificity'] = round(train_tn / (train_tn+train_fp), ROUND)
            res_train['percent_0'] = round((train_fp+train_tn)/(train_tn+train_fp+train_fn+train_tp)*100, ROUND)
            res_train['percent_1'] = 100-res_train['percent_0']
        elif metric == "fpr":
            res_test['fpr'] = round(test_fp / (test_tn+test_fp), ROUND)
            res_train['fpr'] = round(train_fp / (train_tn+train_fp), ROUND)
        elif metric == "mcc":
            print("{}_{}".format((test_tp*test_tn-test_fp*test_fn), (math.sqrt((test_tp+test_fp)*(test_tp+test_fn)*(test_tn+test_fp)*(test_tn+test_fn)))))
            res_test["mcc"] = round((test_tp*test_tn-test_fp*test_fn)/
                                    (math.sqrt((test_tp+test_fp)*(test_tp+test_fn)*(test_tn+test_fp)*(test_tn+test_fn))), ROUND)
            res_train["mcc"] = round((train_tp * train_tn - train_fp * train_fn) /
                                    (math.sqrt((train_tp + train_fp) * (train_tp + train_fn) * (train_tn + train_fp) * (
                                                train_tn + train_fn))), ROUND)
        elif metric == "bcr":
            res_test["bcr"] = round(0.5*(test_tp/(test_fn+test_tp)+test_tn/(test_tn+test_fp)), ROUND)
            res_train["bcr"] = round(0.5*(train_tp/(train_fn+train_tp)+train_tn/(train_tn+train_fp)), ROUND)
    return {'tr': res_train, 't': res_test}

def save_config(suffix_name, all_config):
    column_config, model_config = convert_config(all_config)
    with open(os.path.join(BASE_DIR_CONFIG, suffix_name + "_config_feature"), 'w') as f:
        json.dump(column_config, f, indent=2)
    with open(os.path.join(BASE_DIR_CONFIG, suffix_name + "_config_model"), 'w') as f:
        json.dump(model_config, f, indent=2)

def convert_config(all_config):
    column_config = all_config.get("column_config").copy()
    model_config = all_config.get("model_config")
    d = {}
    if "onehot_cols" in column_config.keys():
        for one_hot_col in column_config.get("onehot_cols"):
            d[one_hot_col] = "one_hot"
        column_config.pop("onehot_cols")
    if "hash_cols" in column_config.keys():
        for col, hash_size in column_config.get("hash_cols").items():
            d[col] = str(hash_size)
        column_config.pop("hash_cols")
    if "emb_cols" in column_config.keys():
        for col, emb_size in column_config.get("emb_cols").items():
            if col in d:
                d[col] = "{}-{}".format(d[col], emb_size)
        column_config.pop("emb_cols")
    column_config["feature_cols"] = d
    return column_config, model_config

def save_res(res, model_config, config):
    #{'accuracy': 1.0, 'accuracy_baseline': 1.0, 'auc': 0.99999905,
    # 'auc_precision_recall': 0.0, 'average_loss': 0.0, 'label/mean': 0.0,
    # 'loss': 0.0, 'precision': 0.0, 'prediction/mean': 0.0, 'recall': 0.0, 'global_step': 16}
    timestamp_name = str(int(time.time()))
    suffix_file_name = "{}_{}".format(path_to_file.split("/")[-1][0:-4], timestamp_name)
    save_config(suffix_file_name, config)
    layer = model_config['layers'][0]
    layer = "x".join([str(unit) for unit in layer])
    rs_show = []
    items = []
    ignore_cols = ['tr_percent_1', 't_percent_1']
    col_show = []
    for name_model, results in res.items():
        col_show = ['model_type']
        row = [name_model]
        item = {'model_type': name_model}
        for type in ['t', 'tr']:
            res_metrics = results.get(type, {})
            for metric, metric_val in res_metrics.items():
                key = "{}_{}".format(type, metric)
                try:
                    item[key] = round(float(metric_val), ROUND)
                except:
                    item[key] = metric_val
                if key not in ignore_cols:
                    col_show.append(key)
                    row.append(item[key])
        items.append(item)
        file_name = "{}_{}_{}_{}_{}.{}".format(path_to_file.split("/")[-1][0:-4], item.get('t_acc'), item.get('t_auc'), name_model, timestamp_name, layer)
        df = pd.DataFrame([item])
        df["file_name"] = file_name
        all_file_results = "all_results.csv"
        if os.path.exists(os.path.join(os.getcwd(), all_file_results)):
            df.to_csv(all_file_results, mode="a", index=False, header=False)
        else:
            df.to_csv(all_file_results, mode="a", index=False)
        rs_show.append(row)
    return col_show, rs_show, suffix_file_name

def read_config_model(config_dir, data_dir):
    all_files = os.listdir(config_dir)
    column_config_files = []
    model_config_files = []
    for file_name in all_files:
        if file_name[-5:] == "model":
            model_config_files.append(file_name)
        else:
            column_config_files.append(file_name)
    all_data_files = os.listdir(data_dir)
    train_files = []
    test_files = []
    for data_file in all_data_files:
        if data_file[-9:-4] == "train":
            train_files.append(data_file)
        else:
            test_files.append(data_file)
    all_data_sets = []
    if len(test_files) > 1:
        for train_file in train_files:
            names = train_file.split("_")
            all_data_sets.append((train_file, "{}_test.csv".format(names[0])))
    else:
        all_data_sets = list(itertools.product(*[train_files, test_files]))
    all_pair_config = list(itertools.product(*[column_config_files, model_config_files, all_data_sets]))
    return all_pair_config

def read_config_model_kfold(config_dir, data_dir):
    all_files = os.listdir(config_dir)
    column_config_files = []
    model_config_files = []
    for file_name in all_files:
        if file_name[-5:] == "model":
            model_config_files.append(file_name)
        else:
            column_config_files.append(file_name)
    all_data_files = os.listdir(data_dir)
    fold_dic = {}
    #"fold1_test.csv"
    for data_file in all_data_files:
        if data_file[-9:-4] == "train":
            fold = data_file.split("_")[-2][4:]
            if fold in fold_dic.keys():
                fold_dic[fold]["tr"] = data_file
            else:
                fold_dic[fold] = {"tr": data_file}
        else:
            fold = data_file.split("_")[-2][4:]
            if fold in fold_dic.keys():
                fold_dic[fold]["t"] = data_file
            else:
                fold_dic[fold] = {"t": data_file}
    data_pair = []
    for fold, val in fold_dic.items():
        data_pair.append((val["tr"], val["t"]))
    return data_pair, (column_config_files[0], model_config_files[0])

def check_success_result(res):
    if (res.get("t").get("auc") >= auc_threshold) and \
            (res.get("t").get("acc") >= acc_threshold) and \
            (res.get("t").get("bcr") >= bcr_threshold) and \
            (res.get("t").get("mcc") >= mcc_threshold):
        return True
    else:
        return False

def check_success_result_multi(res):
    if res.get("t").get("acc") >= acc_threshold:
        return True
    else:
        return False

def save_k_fold_res(k_fold_res,  model_config, config):
    timestamp_name = str(int(time.time()))
    suffix_file_name = "{}_{}".format("k_fold", timestamp_name)
    save_config(suffix_file_name, config)
    layer = model_config['layers'][0]
    layer = "x".join([str(unit) for unit in layer])
    concat_res = {}
    for res in k_fold_res:
        for name_model, results in res.items():
            item = {'model_type': name_model}
            fold_key = results.get("tr").get("fold")
            item["fold"] = fold_key
            for type in ['t', 'tr']:
                res_metrics = results.get(type, {})
                for metric, metric_val in res_metrics.items():
                    if metric == "fold":
                        continue
                    key = "{}_{}".format(type, metric)
                    try:
                        item[key] = round(float(metric_val), ROUND)
                    except:
                        item[key] = metric_val
            file_name = "{}_{}_{}_{}_{}.{}".format(fold_key, item.get('t_acc'), item.get('t_auc'),
                                                   name_model, timestamp_name, layer)
            item["file_name"] = file_name
            if name_model in concat_res:
                concat_res[name_model][fold_key] = item
            else:
                concat_res[name_model] = {fold_key: item}

    for name_model, fold_res in concat_res.items():
        items = []
        for fold_key, item in fold_res.items():
            items.append(item)
        df_res = pd.DataFrame(items)
        df_res["fold_num"] = df_res["fold"].apply(lambda row: int(row.replace("fold", "")))
        df_res.sort_values(by="fold_num", inplace=True)
        summary_row = {}
        ignore_cols = set(["t_percent_0", "t_percent_1", "tr_percent_0", "tr_percent_1", "fold_num"])
        for col in df_res.columns:
            if is_numeric_dtype(df_res[col]) and (col not in ignore_cols):
                summary_row[col] = df_res[col].mean()
            else:
                summary_row[col] = ""
        df_res = df_res.append(summary_row, ignore_index=True)
        df_res.to_csv(os.path.join(BASE_DIR_KFOLD, "{}_{}.csv".format(suffix_file_name, name_model)), index=False)
    return suffix_file_name

if __name__ == "__main__":
    is_kfold = config.get("model.is_kfold")
    data_dir = config.get("model.data_dir")
    tf.logging.set_verbosity(tf.logging.INFO)
    config_dir = config.get("model.config_folder")
    logger.info("Config {}".format(config_dir))

    auc_threshold = config.get("threshold.auc", 0.5)
    acc_threshold = config.get("threshold.acc", 0.5)
    bcr_threshold = config.get("threshold.bcr", 0.5)
    mcc_threshold = config.get("threshold.mcc", 0.1)
    user_ids = config.get("slack.user_id", [])
    if is_kfold:
        notifier = SlackNotifier()
        success_notifier = SlackNotifier(config.get("slack.success_webhook"))
        data_pairs, model_pair = read_config_model_kfold(config_dir, data_dir)
        k_fold_res = []
        config = read_config(os.path.join(config_dir, model_pair[0]), os.path.join(config_dir, model_pair[1]))
        config_columns, model_config, numberic_columns, category_columns, ignore_columns = parser_config(config)
        for data_pair in data_pairs:
            notifier.send_message("Start run {}".format(data_pair))
            path_train_file = os.path.join(data_dir, data_pair[0])
            path_test_file = os.path.join(data_dir, data_pair[1])


            # df_train = pd.read_csv(path_to_file, nrows=100)
            # df_test = pd.read_csv(path_test_file)
            logger.info("Number columns: {}".format(numberic_columns))
            logger.info("Category columns: {}".format(category_columns))
            logger.info("Ignore columns: {}".format(ignore_columns))
            all_cols = config_columns["source_cols"].copy()
            all_cols.append("TARGET")

            df_train = pd.read_csv(path_train_file, dtype=make_dtype(category_columns), usecols=all_cols)
            df_train = shuffle(df_train)
            df_test = pd.read_csv(path_test_file, dtype=make_dtype(category_columns), usecols=all_cols)
            print("Train shape", df_train.shape)
            print("Test shape", df_test.shape)
            print(df_train.head())

            wc, dc = build_model_column(path_train_file, numberic_columns, config_columns)

            res = {}
            for name in model_config.get("model_type"):
                try:
                    if name == "wide" or name == "wd":
                        if len(wc) == 0:
                            continue
                    notifier.send_message("<=======> Start train model: {} <=======>".format(name))
                    model = get_model(name, model_config, wc, dc)
                    time_start = time.time()
                    model.train(input_fn=train_input_fn)
                    time_end = time.time()
                    current_res = calculate_metrics(model_config.get("metrics"), model)
                    current_res["tr"]["fold"] = path_train_file.split("_")[-2]
                    notifier.send_message(
                        "Train model {} in {} hours. Result: {}".format(name, (time_end - time_start) / (60 * 60),
                                                                        current_res))
                    res[name] = current_res
                    if check_success_result_multi(current_res):
                        tmp = ""
                        for user_id in user_ids:
                            tmp = tmp + " <@{}>".format(user_id)
                        success_notifier.send_message(
                            "Train model {} in {} hours. Result: {}".format(name, (time_end - time_start) / (60 * 60),
                                                                            current_res))
                        success_notifier.send_message(tmp)
                    notifier.send_message("<=======> End <=======>")
                except Exception as e:
                    traceback.print_exc()
                    logger.error(e)
                    pass
            k_fold_res.append(res)
        save_k_fold_res(k_fold_res, model_config, config)
    else:
        notifier = SlackNotifier()
        success_notifier = SlackNotifier(config.get("slack.success_webhook", ""))

        all_pair_config = read_config_model(config_dir, data_dir)
        random.shuffle(all_pair_config)
        for pair_config in all_pair_config:
            notifier.send_message("Start run {}".format(pair_config))
            path_to_file = os.path.join(data_dir, pair_config[2][0])
            path_test_file = os.path.join(data_dir, pair_config[2][1])
            config = read_config(os.path.join(config_dir, pair_config[0]), os.path.join(config_dir, pair_config[1]))

            # df_train = pd.read_csv(path_to_file, nrows=100)
            # df_test = pd.read_csv(path_test_file)

            config_columns, model_config, numberic_columns, category_columns, ignore_columns = parser_config(config)
            logger.info("Number columns: {}".format(numberic_columns))
            logger.info("Category columns: {}".format(category_columns))
            logger.info("Ignore columns: {}".format(ignore_columns))
            all_cols = config_columns["source_cols"].copy()
            all_cols.append("TARGET")

            df_train = pd.read_csv(path_to_file, dtype=make_dtype(category_columns), usecols=all_cols)
            df_train = shuffle(df_train)
            df_test = pd.read_csv(path_test_file, dtype=make_dtype(category_columns), usecols=all_cols)
            if len(config_columns.get("feature_engineering")) > 0:
                fe = FeatureEngineer(df_train, config_columns.get("feature_engineering"))
                df_train = fe.transform()
                fe = FeatureEngineer(df_test, config_columns.get("feature_engineering"))
                df_test = fe.transform()

            print("Train shape", df_train.shape)
            print("Test shape", df_test.shape)
            print(df_train.head())

            wc, dc = build_model_column(path_to_file, numberic_columns, config_columns)

            res = {}
            for name in model_config.get("model_type"):
                try:
                    if name == "wide" or name == "wd":
                        if len(wc) == 0:
                            continue
                    notifier.send_message("<=======> Start train model: {} <=======>".format(name))
                    model = get_model(name, model_config, wc, dc)
                    time_start = time.time()
                    model.train(input_fn=train_input_fn)
                    time_end = time.time()
                    current_res = calculate_metrics(model_config.get("metrics"), model)
                    notifier.send_message("Train model {} in {} hours. Result: {}".format(name, (time_end - time_start) / (60 * 60), current_res))
                    res[name] = current_res
                    if check_success_result_multi(current_res):
                        tmp = ""
                        for user_id in user_ids:
                            tmp = tmp + " <@{}>".format(user_id)
                        success_notifier.send_message(
                            "{} Train model {} in {} hours. Result: {}".format(tmp, name, (time_end - time_start) / (60 * 60),
                                                                            current_res))
                        # success_notifier.send_message(tmp)
                    notifier.send_message("<=======> End <=======>")
                except Exception as e:
                    traceback.print_exc()
                    logger.error(e)
                    pass

            save_res(res, model_config, config)

