import os

import toml


def load_config(file_path="./config.toml"):
    env = os.getenv("BKWORKER_ENV", "local")

    if env == "staging":
        file_path = "./config/config_staging.toml"
    elif env == "master":
        file_path = "./config/config_master.toml"
    elif env == "devvm":
        file_path = "./config/config_devvm.toml"
    config = toml.load(file_path)
    return config


class Config():
    def __init__(self):
        self._store = load_config()

    def __getitem__(self, key):
        result = None
        current_dict = self._store
        for k in key.split("."):
            result = current_dict[k]
            current_dict = current_dict[k]
        return result

    def get(self, key, default_val=None):
        try:
            return self.__getitem__(key)
        except:
            # if default_val:
            return default_val
            # raise Exception("Key not found: ", key)

    def getint(self, key) -> int:
        return int(self.get(key))

    def getstring(self,key):
        return str(self.get(key))

    def is_set(self, key):
        is_ok = True
        current_dict = self._store
        for k in key.split("."):
            if k in current_dict:
                current_dict = current_dict[k]
                continue
        return is_ok


config = Config()