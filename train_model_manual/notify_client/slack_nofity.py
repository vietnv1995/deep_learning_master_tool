import requests
import json
from bk_logger import logger
from configloader import config
from retry import retry
from utils import NpEncoder


class SlackNotifier(object):
    def __init__(self, SLACK_LINK_ALERT=None):
        if SLACK_LINK_ALERT is not None:
            self.SLACK_LINK_ALERT = SLACK_LINK_ALERT
        else:
            self.SLACK_LINK_ALERT = config.get("slack.webhook")

    @retry(tries=3, delay=2)
    def send_message(self, message):
        try:
            requests.post(self.SLACK_LINK_ALERT, json.dumps({"text": message}, cls=NpEncoder))
        except Exception as e:
            logger.exception(e)