import os
from bk_logger import logger
import numpy as np
import json

def create_folder(path):
    if not os.path.exists(path):
        os.makedirs(path)

def profiling_df(df_train, train_file, output):
    logger.info("Start profile train file {}".format(train_file))
    profile = df_train.profile_report(title='Pandas Profiling Report')
    profile.to_file(output_file=output)
    logger.info("Done profile train file")

class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NpEncoder, self).default(obj)