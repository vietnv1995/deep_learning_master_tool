import logging
import sys

import logmatic
from configloader import config


class BkLogger(object):
    def __init__(self):
        logger_level = logging.getLevelName(config.get("logger.level", logging.INFO))
        format_str = config.get("logger.pattern", '%(levelname)%(asctime)%(message)')

        handler = logging.StreamHandler(sys.stderr)
        handler.setFormatter(logmatic.JsonFormatter(fmt=format_str))
        self.LOGGER = logging.getLogger(__name__)
        self.LOGGER.setLevel(level=logger_level)
        self.LOGGER.handlers[:] = [handler]

    def info(self, message, *args):
        self.LOGGER.info(message, *args)

    def debug(self, message, *args):
        self.LOGGER.debug(message, *args)

    def warn(self, message, *args):
        self.LOGGER.warning(message, *args)

    def error(self, message, *args):
        self.LOGGER.error(message, *args)

    def exception(self, message, *args):
        self.LOGGER.exception(message, *args)

    def set_level(self, level: str):
        self.LOGGER.setLevel(level.upper())


logger = BkLogger()
