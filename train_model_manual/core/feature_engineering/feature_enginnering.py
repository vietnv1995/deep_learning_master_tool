import numpy as np

class EngineerMethod():
    POLY = 1 # feature1 x feature2. If want feature^2 using feature, feature
    SIN = 2 # sin(feature)
    LOG = 3 # log(feature)

    @classmethod
    def all(cls):
        return [value for name, value in vars(cls).items() if name.isupper()]


class FeatureEngineer():
    def __init__(self, df, feature_engineering):
        '''
        :param df:
        :param transform:
            Format: [
                {method: [features]}
            ]
        '''
        self.feature_engineering = feature_engineering
        self.df = df

    def transform(self):
        for obj in self.feature_engineering:
            method = list(obj.keys())[0]
            self.do(int(method), obj.get(method))
        return self.df

    def do(self, method, features):
        if method == EngineerMethod.POLY:
            self.poly(features)
        elif method == EngineerMethod.SIN:
            self.sin(features[0])
        elif method == EngineerMethod.LOG:
            self.log(features[0])

    def poly(self, features):
        new_col = ""
        result = np.ones(self.df.shape[0])
        for feature in features:
            if new_col == "":
                new_col = "{}".format(feature)
            else:
                new_col = "{}x{}".format(new_col, feature)
            result = result * self.df[feature]
        self.df[new_col] = result

    def sin(self, feature):
        self.df["sin_{}".format(feature)] = np.sin(self.df[feature])

    def log(self, feature):
        self.df["log_{}".format(feature)] = np.log(self.df[feature])

    def get_df(self):
        return self.df


