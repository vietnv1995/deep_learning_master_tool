from imblearn.over_sampling import SMOTE, ADASYN
from imblearn.under_sampling import RandomUnderSampler, NearMiss
from configloader import config
import os
import pandas as pd
from collections import Counter
from bk_logger import logger
import utils

class DataSampler:
    def __init__(self):
        self.config = config
        self.read_config()

    def read_config(self):
        self.algorithms = self.config.get("sampling.algorithm", [])
        self.ratios = self.config.get("sampling.ratio", [0.5])

        self.is_kfold = self.config.get("kfold.is_kfold", False)

        self.data_source = self.config.get("input.data_source", "./data_source")
        self.data_source_sampling = self.config.get("input.data_source_sampling", "./data_sampling")
        utils.create_folder(self.data_source_sampling)

    def execute(self):
        all_file = os.listdir(self.data_source)
        if self.is_kfold:
            datas = {}
            fold_datas = []
            for file in all_file:
                if file[-9:-4] == "train":
                    fold_idx = int(file.split("_")[-2][4:])
                    datas[fold_idx] = datas.get(fold_idx, {})
                    datas[fold_idx]["train"] = file
                else:
                    fold_idx = int(file.split("_")[-2][4:])
                    datas[fold_idx] = datas.get(fold_idx, {})
                    datas[fold_idx]["test"] = file
            for fold_idx, value in datas.items():
                df_train = pd.read_csv(os.path.join(self.data_source, value.get("train")))
                df_test = pd.read_csv(os.path.join(self.data_source, value.get("test")))
                df_test.to_csv(os.path.join(self.data_source_sampling, value.get("test")))
                all_cols = list(df_train.columns)
                all_cols.remove("TARGET")
                X_train = df_train[all_cols].values
                y_train = df_train["TARGET"].values
                X_resampled, y_resampled = self.sampling_data(X_train, y_train, self.algorithms[0], self.ratios[0])
                train_df = pd.DataFrame(X_resampled, columns=all_cols)
                train_df["TARGET"] = y_resampled
                self.save_data(train_df, self.algorithms[0], self.ratios[0], value.get("train"))
            return
        else:
            if len(all_file) == 2:
                if all_file[0][-9:-4] == "train":
                    self.df_train = pd.read_csv(os.path.join(self.data_source, all_file[0]))
                    self.train_path = all_file[0]
                    self.test_path = all_file[1]
                else:
                    self.df_train = pd.read_csv(os.path.join(self.data_source, all_file[1]))
                    self.train_path = all_file[1]
                    self.test_path = all_file[0]
                df_test = pd.read_csv(os.path.join(self.data_source, self.test_path))
                df_test.to_csv(os.path.join(self.data_source_sampling, self.test_path), index=False)
                all_cols = list(self.df_train.columns)
                all_cols.remove("TARGET")
                X_train = self.df_train[all_cols].values
                y_train = self.df_train["TARGET"].values
                for algorithm in self.algorithms:
                    for ratio in self.ratios:
                        X_resampled, y_resampled = self.sampling_data(X_train, y_train, algorithm, ratio)
                        train_df = pd.DataFrame(X_resampled, columns=all_cols)
                        train_df["TARGET"] = y_resampled
                        self.save_data(train_df, algorithm, ratio, self.train_path)
            else:
                raise FileExistsError("Cannot detect train and test file")

    def save_data(self, train_df, algorithm, ratio, file_name):
        data_file = "{}_{}_{}".format(algorithm, ratio, file_name)
        train_df.to_csv(os.path.join(self.data_source_sampling, data_file), index=False)

    def sampling_data(self, X, y, algorithm, ratio):
        sampler = None
        if algorithm == "SMOTE":
            sampler = SMOTE(sampling_strategy=ratio)
        elif algorithm == "ADASYN":
            sampler = ADASYN(sampling_strategy=ratio)
        elif algorithm == "RandomUnderSampler":
            sampler = RandomUnderSampler(sampling_strategy=ratio)
        elif algorithm == "NearMiss1":
            sampler = NearMiss(sampling_strategy=ratio, version=1)
        elif algorithm == "NearMiss2":
            sampler = NearMiss(sampling_strategy=ratio, version=2)
        elif algorithm == "NearMiss3":
            sampler = NearMiss(sampling_strategy=ratio, version=3)
        X_resampled, y_resampled = sampler.fit_resample(X, y)
        logger.info("Algorithm: {}. Ratio: {}".format(algorithm, sorted(Counter(y_resampled).items())))
        return X_resampled, y_resampled

load_default_sampler = DataSampler()