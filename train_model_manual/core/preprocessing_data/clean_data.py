from configloader import config
from sklearn.model_selection import train_test_split
import pandas as pd
from core.preprocessing_data.fill_nan_method import FillNanMethod, FillNanCode
from pandas.api.types import is_numeric_dtype, is_categorical_dtype
from sklearn.preprocessing import StandardScaler, LabelEncoder
from datetime import datetime
from sklearn.model_selection import KFold
import os
import utils
import pandas_profiling
from bk_logger import logger

class DataCleaner:
    def __init__(self):
        self.config = config
        self.read_config()


    def read_config(self):
        self.reject_cols = self.config.get("column_config.reject_cols", [])

        self.train_file = self.config.get("clean_input.train_file", "")
        self.test_file = self.config.get("clean_input.test_file", "")
        self.test_size = self.config.get("clean_input.test_size", 0.3)
        if self.test_size > 1:
            self.kfold = int(self.test_size)
        else:
            self.kfold = 0
        assert self.train_file != "", "Train file cannot null"
        self.df_train = pd.read_csv(self.train_file)
        self.df_train.drop(self.reject_cols, axis=1, inplace=True)
        if self.test_file != "":
            self.df_test = pd.read_csv(self.test_file)
            self.df_test.drop(self.reject_cols, axis=1, inplace=True)

        self.num_cols = self.config.get("column_config.num_cols", [])
        self.cat_cols = self.config.get("column_config.cat_cols", [])
        if len(self.num_cols) == 0 and len(self.cat_cols) == 0:
            self.auto_detect_cols = True
        else:
            self.auto_detect_cols = False

        self.normalize = self.config.get("clean.normalize", ["StandardScaler"])
        self.remove_duplicate = self.config.get("clean.remove_duplicate", True)
        self.fill_nan = self.config.get("clean.fill_nan", [])
        if len(self.fill_nan) > 0:
            if self.fill_nan[0] != "remove":
                self.num_fillnan = self.fill_nan[0].split("-")[0]
                self.cat_fillnan = self.fill_nan[0].split("-")[1]
            else:
                self.df_train.dropna(inplace=True)
                self.fill_nan = []
        self.label_encode = self.config.get("clean.label_encode", True)

        self.data_output = self.config.get("clean_output.data_folder", "./data_source")
        self.data_summary = self.config.get("clean_output.data_summary", "")
        self.only_summary = self.config.get("clean_output.only_summary", False)
        utils.create_folder(self.data_output)
        if self.data_summary != "":
            self.summary = True
            utils.create_folder(self.data_summary)
        else:
            self.summary = False

    def preprocess(self):
        if self.only_summary:
            utils.profiling_df(self.df_train, self.train_file, os.path.join(self.data_summary, "{}.html".format(self.train_file.split("/")[-1][0:-4])))
            return
        if self.kfold > 0:
            logger.info("Start run kfold")
            cv = KFold(n_splits=self.kfold, random_state=42, shuffle=True)
            i = 0
            for train_index, test_index in cv.split(self.df_train):
                i += 1
                # tf.reset_default_graph()
                # with tf.Graph().as_default():
                tr_df, t_df = self.df_train.iloc[train_index], self.df_train.iloc[test_index]
                train_df, test_df = self.clean_data(tr_df, t_df)
                self.save_data(train_df, test_df, i)
            logger.info("Done run kfold")
        else:
            logger.info("Start run train test split")
            if self.test_file == "":
                self.df_train, self.df_test = train_test_split(self.df_train, test_size=self.test_size)
            train_df, test_df = self.clean_data(self.df_train, self.df_test)
            self.save_data(train_df, test_df)
            logger.info("Done run train test split")

    def save_data(self, train_df, test_df, fold=-1):
        now = datetime.now()
        date_time = now.strftime("%m_%d_%Y_%H_%M_%S")
        if fold == -1:
            if len(self.fill_nan) > 0:
                train_file_name = "{}_{}_{}_{}_{}_train.csv".format(date_time, self.remove_duplicate, self.fill_nan[0],
                                                                    self.label_encode, self.normalize[0])
                test_file_name = "{}_{}_{}_{}_{}_test.csv".format(date_time, self.remove_duplicate, self.fill_nan[0],
                                                                    self.label_encode, self.normalize[0])
            else:
                train_file_name = "{}_{}_{}_{}_{}_train.csv".format(date_time, self.remove_duplicate, "notfillna",
                                                                    self.label_encode, self.normalize[0])
                test_file_name = "{}_{}_{}_{}_{}_test.csv".format(date_time, self.remove_duplicate, "notfillna",
                                                                    self.label_encode, self.normalize[0])
        else:
            if len(self.fill_nan) > 0:
                train_file_name = "{}_{}_{}_{}_{}_fold{}_train.csv".format(date_time, self.remove_duplicate, self.fill_nan[0],
                                                                    self.label_encode, self.normalize[0], fold)
                test_file_name = "{}_{}_{}_{}_{}_fold{}_test.csv".format(date_time, self.remove_duplicate, self.fill_nan[0],
                                                                    self.label_encode, self.normalize[0], fold)
            else:
                train_file_name = "{}_{}_{}_{}_{}_fold{}_train.csv".format(date_time, self.remove_duplicate, "notfillna",
                                                                    self.label_encode, self.normalize[0], fold)
                test_file_name = "{}_{}_{}_{}_{}_fold{}_test.csv".format(date_time, self.remove_duplicate, "notfillna",
                                                                    self.label_encode, self.normalize[0], fold)
        train_df.to_csv(os.path.join(self.data_output, train_file_name), index=False)
        test_df.to_csv(os.path.join(self.data_output, test_file_name), index=False)
        logger.info(list(train_df.columns))
        if self.summary:
            utils.profiling_df(train_df, train_file_name, os.path.join(self.data_summary, train_file_name))

    def clean_data(self, tr_df, t_df):
        train_df = tr_df.copy(deep=True)
        test_df = t_df.copy(deep=True)
        if self.remove_duplicate:
            train_df.drop_duplicates(inplace=True)
        if self.auto_detect_cols:
            self.num_cols = []
            self.cat_cols = []
            all_cols = list(train_df.columns)
            all_cols.remove("TARGET")
            for col in all_cols:
                if is_numeric_dtype(train_df[col]):
                    self.num_cols.append(col)
                else:
                    self.cat_cols.append(col)
            logger.info("Cat cols: {}".format(self.cat_cols))
        if len(self.fill_nan) > 0:
            col_fillnas = {}

            fill_code = "mode"
            if self.num_fillnan == "mean":
                fill_code = FillNanCode.FillMean
            elif self.num_fillnan == "mode":
                fill_code = FillNanCode.FillMode
            elif self.num_fillnan == "zero":
                fill_code = FillNanCode.FillZero
            for num_col in self.num_cols:
                col_fillnas[num_col] = fill_code

            if self.cat_fillnan == "mode":
                fill_code = FillNanCode.FillMode
            elif self.cat_fillnan == "zero":
                fill_code = FillNanCode.FillZero
            for cat_col in self.cat_cols:
                col_fillnas[cat_col] = fill_code

            fillna_obj = FillNanMethod(train_df, test_df)
            fillna_obj.fillnans(col_fillnas)
            train_df, test_df = fillna_obj.get_result()
        if self.label_encode:
            for cat_col in self.cat_cols:
                set_values = list(set(train_df[cat_col]))
                set_values.append("unknown")
                lb = LabelEncoder()
                lb.fit(set_values)
                set_values = set(set_values)
                test_df[cat_col] = test_df[cat_col].apply(lambda row: row if row in set_values else "unknown")
                train_df[cat_col] = lb.transform(train_df[cat_col])
                test_df[cat_col] = lb.transform(test_df[cat_col])

        if len(self.normalize) > 0:
            for num_col in self.num_cols:
                scaler = StandardScaler()
                train_df[num_col] = scaler.fit_transform(train_df[num_col].values.reshape(-1, 1))[:, 0]
                test_df[num_col] = scaler.transform(test_df[num_col].values.reshape(-1, 1))[:, 0]
        return train_df, test_df

load_default_cleaner = DataCleaner()