from configloader import config
from bk_logger import logger
import itertools
import random
import os, json
from core.feature_engineering.feature_enginnering import EngineerMethod

class GenerateStrategy:
    MODEL = "model"
    FEATURE = "feature"

    # in feature
    ONEHOT = 1
    HASH = 2
    EMB = 1

class ConfigGenerate:
    def __init__(self):
        # strategy: []
        self.config = config
        self.strategy = self.config.get("auto_generate.strategy")
        self.output_folder = self.config.get("auto_generate.output_folder", "./config_generate")
        self.create_folder(self.output_folder)
        logger.info("Generate strategy: {}. Output: {}".format(self.strategy, self.output_folder))

    def generate(self):
        if self.strategy == GenerateStrategy.MODEL:
            self.model_generate()
            return
        elif self.strategy == GenerateStrategy.FEATURE:
            self.feature_generate()
            return

    # model config
    def model_generate(self):
        self.model_generate_read_params()
        self.set_choose = set()
        cnt = 0
        for i in range(self.limit_number_config):
            model_config = self.pick_one_choose_model()
            if model_config is not None:
                cnt += 1
                self.save_model_config(model_config)
        logger.info("Generate {} config files".format(cnt))

    def create_folder(self, path):
        if not os.path.exists(path):
            os.makedirs(path)

    def save_model_config(self, model_config):
        file_name = "{}_{}_{}_{}_{}_{}".format(model_config["nn"]["layers"],
                                            model_config["nn"]["dropouts"],
                                            model_config["epochs"],
                                            model_config["optimizer"],
                                            model_config["activation"],
                                               self.strategy)
        with open(os.path.join(*[self.output_folder, file_name]), "w") as f:
            json.dump(model_config, f, indent=2)

    def model_generate_read_params(self):
        logger.info("Start read params")
        self.limit_number_config = self.config.get("auto_generate.limit_number_config", 20)
        self.layers_choose = self.config.get("auto_generate.layers", [500, 400, 300, 200, 100])
        self.number_layers = self.config.get("auto_generate.number_layers", [3, 4])
        self.dropouts = self.config.get("auto_generate.dropouts", [0])
        self.epochs = self.config.get("auto_generate.epochs", [50, 100, 200, 500])
        self.optimizer = self.config.get("auto_generate.optimizer", ["adam", "ada"])
        self.activation = self.config.get("auto_generate.activation", ["relu"])
        self.metrics = self.config.get("auto_generate.metrics", ["auc", "acc", "confus", "precision", "recall", "fpr", "f1_score", "specificity", "logloss", "mcc", "bcr"])
        self.model_type = self.config.get("auto_generate.model_type", ["deep"])
        self.learning_rate = self.config.get("auto_generate.learning_rate", [0.05])
        self.layers = []
        for n_layer in self.number_layers:
            tmp = [self.layers_choose] * n_layer
            tmp1 = itertools.product(*tmp)
            self.layers.extend(tmp1)
        if self.epochs[0] == 0:
            self.epochs = [item for item in range(self.epochs[1], self.epochs[2])]
        logger.info("Done read params")

    def pick_one_choose_model(self):
        '''
        Random layer with condition unit layer[0] > layer[1]
        Random dropout with condition if random equal len will remove dropout
        :return:
        '''
        retry = 0
        while True:
            while True:
                layer_idx = random.randint(0, len(self.layers)-1)
                if self.layers[layer_idx][0] > self.layers[layer_idx][1]:
                    break
            dropout_idx = random.randint(0, len(self.dropouts))
            epoch_idx = random.randint(0, len(self.epochs) - 1)
            optimizer_idx = random.randint(0, len(self.optimizer) - 1)
            activation_idx = random.randint(0, len(self.activation) - 1)
            learning_rate_idx = random.randint(0, len(self.learning_rate) - 1)
            final_pick = "{}{}{}{}{}{}".format(layer_idx, dropout_idx, epoch_idx, optimizer_idx,
                                               activation_idx, learning_rate_idx)
            if final_pick in self.set_choose:
                retry += 1
                if retry >= 10:
                    return None
                continue
            else:
                self.set_choose.add(final_pick)
                if dropout_idx == len(self.dropouts):
                    dropout = 0
                else:
                    dropout = float(self.dropouts[dropout_idx])
                model_config = {"nn": {
                                    "layers": "x".join([str(item) for item in self.layers[layer_idx]]),
                                    "dropouts": dropout
                                },
                                "loss_function": "binary_crossentropy",
                                "epochs": self.epochs[epoch_idx],
                                "optimizer": self.optimizer[optimizer_idx],
                                "learning_rate": self.learning_rate[learning_rate_idx],
                                "activation": self.activation[activation_idx],
                                "test_size": 0.3,
                                "shuffle": 5000,
                                "metrics": self.metrics,
                                "model_type": self.model_type
                                }
                return model_config

    # feature config
    def feature_generate(self):
        self.feature_generate_read_params()
        self.set_feature_choose = set()
        cnt = 0
        for i in range(self.limit_number_config):
            final_pick, feature_config = self.pick_one_choose_feature()
            if feature_config is not None:
                cnt += 1
                self.save_feature_config(final_pick, feature_config)
        logger.info("Generate {} feature config files".format(cnt))

    def save_feature_config(self, final_pick, feature_config):
        file_name = "ignore_{}_wide_{}_{}_{}".format("_".join(feature_config["ignore_cols"]),
                                               "_".join(feature_config["wide_cols"]),
                                                     final_pick,
                                               self.strategy)
        with open(os.path.join(*[self.output_folder, file_name]), "w") as f:
            json.dump(feature_config, f, indent=2)


    def feature_generate_read_params(self):
        logger.info("Start read params")
        self.limit_number_config = self.config.get("auto_generate.limit_number_config", 20)
        self.all_cols = self.config.get("auto_generate.all_cols", [])
        self.deep_cols = self.config.get("auto_generate.deep_cols", [])
        self.ignore_cols = self.config.get("auto_generate.ignore_cols", [])
        self.cat_cols = self.config.get("auto_generate.cat_cols", [])
        self.hash_size = self.config.get("auto_generate.hash_size", [10000, 20000])
        self.emb_size = self.config.get("auto_generate.emb_size", [25, 100])
        self.wide_cols_options = self.config.get("auto_generate.wide_cols", [])
        self.cross_cols = self.config.get("auto_generate.cross_cols", [])
        self.wide_cols_options = list(set(self.wide_cols_options).union(set(self.cross_cols)))
        self.feature_engineering = self.config.get("auto_generate.feature_engineering", False)
        logger.info("Done read params")

    def pick_one_choose_feature(self):
        retry = 0
        while True:
            # if random = len(ignore_cols) => no ignore
            ignore_col_idx = random.randint(0, len(self.ignore_cols))
            cat_keys = ""
            feature_cols = {}
            for cat_col in self.cat_cols:
                check = random.randint(1, 2)
                if check == GenerateStrategy.ONEHOT:
                    tmp = "one_hot"
                else:
                    tmp = str(random.choice([item for item in range(self.hash_size[0], self.hash_size[1]) if item % 8 ==0 ]))
                check = random.randint(0, 1)
                if check == GenerateStrategy.EMB:
                    tmp = tmp + "-{}".format(random.choice([item for item in range(self.emb_size[0], self.emb_size[1]) if item % 8 ==0 ]))
                cat_keys = cat_keys + "_{}".format(tmp)
                feature_cols[cat_col] = tmp
            final_pick = "{}{}".format(ignore_col_idx, cat_keys)
            if final_pick in self.set_feature_choose:
                retry += 1
                if retry >= 10:
                    return None, None
                continue
            else:
                self.set_feature_choose.add(final_pick)
                if len(self.wide_cols_options) > 1:
                    wide_cols = random.sample(self.wide_cols_options, random.randint(1, len(self.wide_cols_options) - 1))
                elif len(self.wide_cols_options) == 1:
                    wide_cols = random.sample(self.wide_cols_options,
                                              random.randint(0, len(self.wide_cols_options) - 1))
                else:
                    wide_cols = []
                if len(self.cross_cols) == 1:
                    wide_cols.append(self.cross_cols[0])
                    wide_cols = list(set(wide_cols))
                deep_cols = self.deep_cols
                ignore_cols = []
                if ignore_col_idx != len(self.ignore_cols):
                    if self.ignore_cols[ignore_col_idx] in set(self.deep_cols):
                        deep_cols = list(set(self.deep_cols) - set(self.ignore_cols[ignore_col_idx]))
                        ignore_cols = [self.ignore_cols[ignore_col_idx]]
                list_feature_engineering = []
                deep_cols_new = deep_cols.copy()
                all_cols_new = self.all_cols.copy()
                if self.feature_engineering:
                    deep_cols_copy = deep_cols.copy()
                    number_new_feature = random.randint(3, 6)
                    all_method = EngineerMethod.all()
                    fe_check = set([])
                    while len(fe_check) != number_new_feature:
                        new_col = ""
                        feature_engineering = {}
                        method = all_method[random.randint(0, len(all_method)-1)]
                        val = []
                        if method == EngineerMethod.POLY:
                            for j in range(random.randint(2, 4)):
                                col_choose = self.get_random_list(deep_cols_copy)
                                val.append(col_choose)
                                if new_col == "":
                                    new_col = "{}".format(col_choose)
                                else:
                                    new_col = "{}x{}".format(new_col, col_choose)
                        elif method == EngineerMethod.SIN:
                            col_choose = self.get_random_list(deep_cols_copy)
                            new_col = "sin_{}".format(col_choose)
                            val.append(col_choose)
                        elif method == EngineerMethod.LOG:
                            col_choose = self.get_random_list(deep_cols_copy)
                            new_col = "log_{}".format(col_choose)
                            val.append(col_choose)
                        if new_col not in fe_check:
                            feature_engineering[method] = val
                            deep_cols_new.append(new_col)
                            all_cols_new.append(new_col)
                            list_feature_engineering.append(feature_engineering)
                            fe_check.add(new_col)
                feature_config = {"wide_cols": wide_cols,
                                  "deep_cols": deep_cols_new,
                                  "ignore_cols": ignore_cols,
                                  "cross_cols": {item: random.choice(range(self.hash_size[0], self.hash_size[1], 8)) for item in self.cross_cols},
                                  "all_cols": all_cols_new,
                                  "source_cols": self.all_cols,
                                  "cat_cols": self.cat_cols,
                                  "num_cols": list(set(all_cols_new)- set(self.cat_cols)),
                                  "feature_cols": feature_cols,
                                  "feature_engineering": list_feature_engineering}
                return final_pick, feature_config
    def get_random_list(self, arr):
        return arr[random.randint(0, len(arr)-1)]
load_default_auto_generate = ConfigGenerate()



