

class FillNanCode(object):
    FillMean = 1
    FillMode = 2
    FillZero = 3

class FillNanMethod(object):
    def __init__(self, df_train, df_test=None):
        self.df_train = df_train
        self.df_test = df_test

    def fillnan(self, method, col_name):
        if self.df_test is None:
            if method == FillNanCode.FillMean:
                self.df_train[col_name].fillna(self.df_train[col_name].mean(), inplace=True)
            if method == FillNanCode.FillMode:
                self.df_train[col_name].fillna(self.df_train[col_name].mode()[0], inplace=True)
            if method == FillNanCode.FillZero:
                self.df_train[col_name].fillna(0, inplace=True)
        else:
            if method == FillNanCode.FillMean:
                self.df_train[col_name].fillna(self.df_train[col_name].mean(), inplace=True)
                self.df_test[col_name].fillna(self.df_train[col_name].mean(), inplace=True)
            if method == FillNanCode.FillMode:
                self.df_train[col_name].fillna(self.df_train[col_name].mode()[0], inplace=True)
                self.df_test[col_name].fillna(self.df_train[col_name].mode()[0], inplace=True)
            if method == FillNanCode.FillZero:
                self.df_train[col_name].fillna(0, inplace=True)
                self.df_test[col_name].fillna(0, inplace=True)

    def fillnans(self, col_fillnas):
        '''
        :param col_fillnas: dict {col_name: method}
        :return:
        '''
        for k, v in col_fillnas.items():
            self.fillnan(v, k)

    def get_result(self):
        return self.df_train, self.df_test