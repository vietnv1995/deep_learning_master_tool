import tensorflow as tf
import pandas as pd


class DataTable(object):
    def __init__(self, path_to_file, column_types, column_cats, col_types_name, uv):
        '''

        :param path_to_file:
        :param column_type: dict { column_name: type, ... }
        '''
        self.path_to_file = path_to_file
        self.columns = list(column_types.keys())
        self.types = list(column_types.values())
        self.column_types = column_types
        self.column_cats = column_cats
        self.column_types_name = col_types_name
        self.uv = uv
        self.config_columns = None
        self.numberic_columns = [item for item in self.columns if item not in self.column_cats]

    def input_fn(self, num_epochs, shuffle, batch_size):
        assert tf.gfile.Exists(self.path_to_file), ('%s khong ton tai.' % self.path_to_file)
        df = pd.read_csv(self.path_to_file, nrows=1)
        new_columns = df.columns.to_list()
        new_types = [self.column_types.get(column, [0]) for column in new_columns]

        def parse_csv(value):
            print('Parsing', self.path_to_file)
            types = tf.decode_csv(value, record_defaults=new_types)
            features = dict(zip(new_columns, types))
            labels = features.pop('TARGET')
            return features, tf.equal(labels, 1)

        dataset = tf.data.TextLineDataset(self.path_to_file).skip(1)

        if shuffle!=0:
            dataset = dataset.shuffle(buffer_size=shuffle)

        dataset = dataset.map(parse_csv, num_parallel_calls=5)
        dataset = dataset.repeat(num_epochs)
        dataset = dataset.batch(batch_size)
        return dataset

    def get_target(self):
        df_target = pd.read_csv(self.path_to_file, usecols=['TARGET'])
        return df_target['TARGET'].tolist()

    def set_config_columns(self, config):
        self.config_columns = config

    def get_config_columns(self):
        return self.config_columns

    def get_numberic_columns(self):
        # {'name': 'id', 'type': 'int', 'n_unique': 1000, 'is_wide': 0, 'is_deep': 1, 'is_ignore': 0}


        if self.config_columns is not None:
            res = []
            for name in self.numberic_columns:
                d = {'name': name, 'is_wide': 0, 'is_deep': 0, 'is_ignore': 0}
                d['type'] = self.column_types_name[name]
                d['n_unique'] = self.uv.get(name)
                if name in self.config_columns['wide_cols']:
                    d['is_wide'] = 1
                if name in self.config_columns['deep_cols']:
                    d['is_deep'] = 1
                if name in self.config_columns['ignore_cols']:
                    d['is_ignore'] = 1
                res.append(d)
        else:
            res = [{'name': name,
                     'type': self.column_types_name[name],
                     'n_unique': self.uv.get(name),
                     'is_wide': 0,
                     'is_deep': 1,
                     'is_ignore': 0} for name in self.numberic_columns]
        res = sorted(res, key=lambda k: k['name'])
        return res

    def get_categorical_columns(self):
        # {'name': 'education', 'type': 'string', 'n_unique': 100, 'encode': 'onehot|hash',
        # 'hash_size': 1000, 'is_emb': 1, 'emb_size': 50, 'is_wide': 0, 'is_deep': 1, 'is_ignore': 0}
        if self.config_columns is not None:
            res = []
            for name in self.column_cats:
                d = {'name': name, 'is_wide': 0, 'is_deep': 0, 'is_ignore': 0}
                d['type'] = self.column_types_name[name]
                d['n_unique'] = self.uv.get(name)
                if name in self.config_columns['wide_cols']:
                    d['is_wide'] = 1
                if name in self.config_columns['deep_cols']:
                    d['is_deep'] = 1
                if name in self.config_columns['ignore_cols']:
                    d['is_ignore'] = 1
                if name in self.config_columns['onehot_cols']:
                    d['encode'] = 'onehot'
                else:
                    d['encode'] = 'hash'
                if name in self.config_columns['hash_cols'].keys():
                    d['hash_size'] = self.config_columns['hash_cols'][name]
                if name in self.config_columns['emb_cols'].keys():
                    d['is_emb'] = 1
                    d['emb_size'] = self.config_columns['emb_cols'][name]
                else:
                    d['is_emb'] = 0
                res.append(d)
        else:
            res = [{'name': name,
                     'type': self.column_types_name[name],
                     'n_unique': self.uv.get(name),
                     'encode': 'onehot',
                     'is_emb': 0,
                     'is_wide': 0,
                     'is_deep': 1,
                     'is_ignore': 0} for name in self.column_cats]
        res = sorted(res, key=lambda k: k['name'])
        return res

    def get_cross_columns(self):
        res = []
        if self.config_columns is not None:
            for name, hash_size in self.config_columns['cross_cols'].items():
                d = {'name': name, 'hash_size': hash_size, 'is_wide': 0, 'is_deep': 0, 'is_ignore': 0}
                if name in self.config_columns['wide_cols']:
                    d['is_wide'] = 1
                if name in self.config_columns['deep_cols']:
                    d['is_deep'] = 1
                if name in self.config_columns['ignore_cols']:
                    d['is_ignore'] = 1
                res.append(d)
        res = sorted(res, key=lambda k: k['name'])
        return res

    def build_model_column(self):
        '''
        Notice that one hot column and hash column must use through indicator column
        :return:
        '''
        numberic_dict = {}
        for name in self.numberic_columns:
            numberic_dict[name] = tf.feature_column.numeric_column(name)
        onehot_dict = {}
        for name in self.config_columns['onehot_cols']:
            df = pd.read_csv(self.path_to_file, usecols=[name], dtype="str")
            onehot_dict[name] = tf.feature_column.categorical_column_with_vocabulary_list(
                name, df[name].unique().tolist()
            )
        hash_dict = {}
        for name, hash_size in self.config_columns['hash_cols'].items():
            hash_dict[name] = tf.feature_column.categorical_column_with_hash_bucket(
                name, hash_bucket_size=hash_size)
        emb_dict = {}
        for name, emb_size in self.config_columns['emb_cols'].items():
            if name in onehot_dict.keys():
                emb_dict[name] = tf.feature_column.embedding_column(
                    onehot_dict.get(name), dimension=emb_size)
            else:
                emb_dict[name] = tf.feature_column.embedding_column(
                    hash_dict.get(name), dimension=emb_size)
        cross_dict = {}
        # coss_cols have format col1_name:100 x col2_name:200.
        # We must split by " x " and remove number of uv after :
        for name, hash_size in self.config_columns['cross_cols'].items():
            names = name.split(" x ")
            names = [":".join(item.split(":")[0:-1]) for item in names]
            cross_dict[name] = tf.feature_column.crossed_column(names, hash_size)
        wide_columns = []
        deep_columns = []
        print("Wide: {}".format(self.config_columns['wide_cols']))
        print("Deep: {}".format(self.config_columns['deep_cols']))
        for name in self.config_columns['wide_cols']:
            if name in cross_dict.keys():
                wide_columns.append(cross_dict[name])
            elif name in numberic_dict.keys():
                wide_columns.append(numberic_dict.get(name))
            elif name in onehot_dict.keys():
                wide_columns.append(onehot_dict.get(name))
            else:
                wide_columns.append(hash_dict.get(name))
        for name in self.config_columns['deep_cols']:
            if name in cross_dict.keys():
                deep_columns.append(tf.feature_column.indicator_column(cross_dict[name]))
            elif name in numberic_dict.keys():
                deep_columns.append(numberic_dict.get(name))
            elif name in emb_dict.keys():
                deep_columns.append(emb_dict.get(name))
            elif name in onehot_dict.keys():
                deep_columns.append(tf.feature_column.indicator_column(onehot_dict.get(name)))
            else:
                deep_columns.append(tf.feature_column.indicator_column(hash_dict.get(name)))
        return wide_columns, deep_columns




