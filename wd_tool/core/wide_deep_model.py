import tensorflow as tf
import logging
from dl_tool.core.const import ROUND
from sklearn import metrics
import pandas as pd
from sklearn.model_selection import KFold
import shutil
import math

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)

class WideDeepModel(object):


    def __init__(self, train, test, config_model, model_dir, name='all'):
        self.name = name
        self.train = train
        self.test = test
        self.wide_columns, self.deep_columns = self.train.build_model_column()
        self.run_config = tf.estimator.RunConfig().replace(
            session_config=tf.ConfigProto(device_count={'GPU': 0}))
        self.config_model = config_model
        self.learning_rate = 0.05
        self.batch_size = 32
        self.epochs_between_evals = config_model.get("epochs", 1)
        self.df_train = None
        self.df_test = None
        self.model_dir = model_dir
        self.shuffle = config_model.get("shuffle", 0)
        tf.logging.set_verbosity(tf.logging.INFO)

    def get_model(self, name):
        tf.reset_default_graph()
        shutil.rmtree(self.model_dir, ignore_errors=True)
        if name == 'wide':
            return tf.estimator.LinearClassifier(
                model_dir=self.model_dir,
                feature_columns=self.wide_columns,
                config=self.run_config)
        elif name == 'deep':
            return tf.estimator.DNNClassifier(
            model_dir=self.model_dir,
            feature_columns=self.deep_columns,
            hidden_units=list(self.config_model.get('layers')[0]),
            optimizer=self.get_optimizer(),
            config=self.run_config)
        elif name == 'wd':
            return tf.estimator.DNNLinearCombinedClassifier(
            model_dir=self.model_dir,
            linear_feature_columns=self.wide_columns,
            dnn_feature_columns=self.deep_columns,
            dnn_hidden_units=list(self.config_model.get('layers')[0]),
            dnn_optimizer=self.get_optimizer(),
            config=self.run_config)
        else:
            return None
            # params = {
            #     'n_trees': 100,
            #     'max_depth': 6,
            #     'n_batches_per_layer': 1,
            #     # You must enable center_bias = True to get DFCs. This will force the model to
            #     # make an initial prediction before using any features (e.g. use the mean of
            #     # the training labels for regression or log odds for classification when
            #     # using cross entropy loss).
            #     'center_bias': True
            # }
            # return tf.estimator.BoostedTreesClassifier(
            # model_dir=self.model_dir,
            # feature_columns=self.deep_columns,
            # **params)

    def get_optimizer(self):
        optimizer_name = self.config_model.get("optimizer")
        if optimizer_name == "ada":
            return tf.train.AdagradOptimizer(learning_rate=self.learning_rate)
        elif optimizer_name == "adam":
            return tf.train.AdamOptimizer(learning_rate=self.learning_rate)
        elif optimizer_name == "rms":
            return tf.train.RMSPropOptimizer(learning_rate=self.learning_rate)

    def train_input_fn(self):
        return self.train.input_fn(self.epochs_between_evals, self.shuffle, self.batch_size)

    def train_input_fn_one_epoch(self):
        return self.train.input_fn(1, 0, self.batch_size)

    def eval_input_fn(self):
        return self.test.input_fn(1, 0, self.batch_size)

    def df_to_dataset(self, dataframe, num_epochs, shuffle, batch_size=32):
        dataframe = dataframe.copy()
        labels = dataframe.pop('TARGET')
        ds = tf.data.Dataset.from_tensor_slices((dict(dataframe), labels))
        if shuffle != 0:
            ds = ds.shuffle(buffer_size=shuffle)
        ds = ds.repeat(num_epochs)
        ds = ds.batch(batch_size)
        return ds

    def fold_train_input_fn(self):
        return self.df_to_dataset(self.df_train, self.epochs_between_evals, self.shuffle, self.batch_size)

    def fold_train_one_input_fn(self):
        return self.df_to_dataset(self.df_train, 1, 0, self.batch_size)

    def fold_test_input_fn(self):
        return self.df_to_dataset(self.df_test, 1, 0, self.batch_size)

    def fit_with_kfold(self, n_folds, model_config):
        col_types = {}
        for k, v in self.train.column_types_name.items():
            if v == 'int':
                col_types[k] = int
            elif v == 'float':
                col_types[k] = float
            else:
                col_types[k] = 'str'
        df = pd.read_csv(self.train.path_to_file, dtype=col_types)
        cv = KFold(n_splits=n_folds, random_state=42, shuffle=True)
        i = 0
        fold_res = []
        for train_index, test_index in cv.split(df):
            i+=1
            # tf.reset_default_graph()
            # with tf.Graph().as_default():
            self.df_train, self.df_test = df.iloc[train_index], df.iloc[test_index]

            res = {}
            model_types = model_config.get("model_type")
            metrics = model_config.get("metrics")
            for name in model_types:
                logging.info("Start fold {} for model {}".format(i, name))
                model = self.get_model(name)
                model.train(input_fn=self.fold_train_input_fn)
                results = model.evaluate(input_fn=self.fold_test_input_fn)
                print("Fold {}: {}".format(i, results))
                current_res = self.calculate_metrics(metrics, model, kfold=True)
                res[name] = current_res
            fold_res.append(res)
            # res have format {'tr': {'acc': }, 't': {'acc': }}
        return fold_res

    def fit(self, model_config):
        res = {}
        model_types = model_config.get("model_type")
        metrics = model_config.get("metrics")
        for name in model_types:
            logging.info("Start train model: {}".format(name))
            model = self.get_model(name)
            model.train(input_fn=self.train_input_fn)
            # results = model.evaluate(input_fn=self.eval_input_fn)
            current_res = self.calculate_metrics(metrics, model)
            res[name] = current_res
        return res

    def calculate_metrics(self, list_metrics, model, kfold=False):
        res_train = {}
        res_test = {}


        y_train_real = self.train.get_target()
        y_test_real = self.test.get_target()
        if kfold:
            y_train_real = self.df_train['TARGET'].values
            y_test_real = self.df_test['TARGET'].values
            test_pred_gens = model.predict(input_fn=self.fold_test_input_fn)
            results = model.evaluate(input_fn=self.fold_test_input_fn)
            res_train['loss'] = results.get('average_loss')
        else:
            test_pred_gens = model.predict(input_fn=self.eval_input_fn)
            results = model.evaluate(input_fn=self.eval_input_fn)
            res_train['loss'] = results.get('average_loss')
        y_test_pred = []
        y_test_classes = []
        for item in test_pred_gens:
            y_test_pred.append(item['probabilities'][1])
            y_test_classes.append(item['class_ids'][0])

        if kfold:
            train_pred_gens = model.predict(input_fn=self.fold_train_one_input_fn)
        else:
            train_pred_gens = model.predict(input_fn=self.train_input_fn_one_epoch)
        y_train_pred = []
        y_train_classes = []
        for item in train_pred_gens:
            y_train_pred.append(item['probabilities'][1])
            y_train_classes.append(item['class_ids'][0])
        test_tn, test_fp, test_fn, test_tp = metrics.confusion_matrix(y_test_real, y_test_classes).ravel()
        train_tn, train_fp, train_fn, train_tp = metrics.confusion_matrix(y_test_real, y_test_classes).ravel()
        for metric in list_metrics:
            if metric == 'auc':
                fpr, tpr, thresholds = metrics.roc_curve(y_test_real, y_test_pred, pos_label=1)
                res_test['auc'] = round(metrics.auc(fpr, tpr), ROUND)

                fpr, tpr, thresholds = metrics.roc_curve(y_train_real, y_train_pred, pos_label=1)
                res_train['auc'] = round(metrics.auc(fpr, tpr), ROUND)
            elif metric == 'acc':
                res_test['acc'] = round(metrics.accuracy_score(y_test_real, y_test_classes), ROUND)
                res_train['acc'] = round(metrics.accuracy_score(y_train_real, y_train_classes), ROUND)
            elif metric == 'confus':
                print(metrics.confusion_matrix(y_test_real, y_test_classes))
                res_test['confus'] = str([list(item) for item in metrics.confusion_matrix(y_test_real, y_test_classes)])
                res_train['confus'] = str([list(item) for item in metrics.confusion_matrix(y_train_real, y_train_classes)])
            elif metric == 'precision':
                res_test['precision'] = round(metrics.precision_score(y_test_real, y_test_classes), ROUND)
                res_train['precision'] = round(metrics.precision_score(y_train_real, y_train_classes), ROUND)
            elif metric == 'recall':
                res_test['recall'] = round(metrics.recall_score(y_test_real, y_test_classes), ROUND)
                res_train['recall'] = round(metrics.recall_score(y_train_real, y_train_classes), ROUND)
            elif metric == 'f1_score':
                res_test['f1'] = round(metrics.f1_score(y_test_real, y_test_classes), ROUND)
                res_train['f1'] = round(metrics.f1_score(y_train_real, y_train_classes), ROUND)
            elif metric == 'specificity':
                res_test['specificity'] = round(test_tn / (test_tn+test_fp), ROUND)
                res_test['percent_0'] = round((test_fp + test_tn) / (test_tn + test_fp + test_fn + test_tp)*100, ROUND)
                res_test['percent_1'] = 100-res_test['percent_0']


                res_train['specificity'] = round(train_tn / (train_tn+train_fp), ROUND)
                res_train['percent_0'] = round((train_fp+train_tn)/(train_tn+train_fp+train_fn+train_tp)*100, ROUND)
                res_train['percent_1'] = 100-res_train['percent_0']
            elif metric == "fpr":
                res_test['fpr'] = round(test_fp / (test_tn+test_fp), ROUND)
                res_train['fpr'] = round(train_fp / (train_tn+train_fp), ROUND)
            elif metric == "mcc":
                print("{}_{}".format((test_tp*test_tn-test_fp*test_fn), (math.sqrt((test_tp+test_fp)*(test_tp+test_fn)*(test_tn+test_fp)*(test_tn+test_fn)))))
                res_test["mcc"] = round((test_tp*test_tn-test_fp*test_fn)/
                                        (math.sqrt((test_tp+test_fp)*(test_tp+test_fn)*(test_tn+test_fp)*(test_tn+test_fn))), ROUND)
                res_train["mcc"] = round((train_tp * train_tn - train_fp * train_fn) /
                                        (math.sqrt((train_tp + train_fp) * (train_tp + train_fn) * (train_tn + train_fp) * (
                                                    train_tn + train_fn))), ROUND)
            elif metric == "bcr":
                res_test["bcr"] = round(0.5*(test_tp/(test_fn+test_tp)+test_tn/(test_tn+test_fp)), ROUND)
                res_train["bcr"] = round(0.5*(train_tp/(train_fn+train_tp)+train_tn/(train_tn+train_fp)), ROUND)
        return {'tr': res_train, 't': res_test}

