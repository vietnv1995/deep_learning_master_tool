from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse

import tensorflow as tf

class BaseParser(argparse.ArgumentParser):

  def __init__(self, add_help=False, data_dir=True, model_dir=True,
               train_epochs=True, epochs_between_evals=True, batch_size=True,
               multi_gpu=True, hooks=True):
    super(BaseParser, self).__init__(add_help=add_help)

    if data_dir:
      self.add_argument(
          "--data_dir", "-dd", default="/tmp",
          help="[default: %(default)s] Duong dan toi du lieu dau vao.",
          metavar="<DD>",
      )

    if model_dir:
      self.add_argument(
          "--model_dir", "-md", default="/tmp",
          help="[default: %(default)s] Duong dan luu mo hinh.",
          metavar="<MD>",
      )

    if train_epochs:
      self.add_argument(
          "--train_epochs", "-te", type=int, default=1,
          help="[default: %(default)s] So epoch train.",
          metavar="<TE>"
      )

    if epochs_between_evals:
      self.add_argument(
          "--epochs_between_evals", "-ebe", type=int, default=1,
          help="[default: %(default)s] So epoch giua moi lan kiem dinh.",
          metavar="<EBE>"
      )

    if batch_size:
      self.add_argument(
          "--batch_size", "-bs", type=int, default=32,
          help="[default: %(default)s] Kich thuoc tap du lieu con.",
          metavar="<BS>"
      )

    if multi_gpu:
      self.add_argument(
          "--multi_gpu", action="store_true",
          help="Cho phep chay tren GPUs."
      )

    if hooks:
      self.add_argument(
          "--hooks", "-hk", nargs="+", default=["LoggingTensorHook"],
          help="[default: %(default)s] Danh sach ten hooks"
               "VD: --hooks LoggingTensorHook ExamplesPerSecondHook. "
               "Cac gia tri kha dung: LoggingTensorHook, "
               "ProfilerHook, ExamplesPerSecondHook. ",
          metavar="<HK>"
      )

class ExamplesPerSecondHook(tf.train.SessionRunHook):
  def __init__(self,
               batch_size,
               every_n_steps=None,
               every_n_secs=None,
               warm_steps=0):

    if (every_n_steps is None) == (every_n_secs is None):
      raise ValueError('exactly one of every_n_steps'
                       ' and every_n_secs should be provided.')
    self._timer = tf.train.SecondOrStepTimer(
        every_steps=every_n_steps, every_secs=every_n_secs)
    self._step_train_time = 0
    self._total_steps = 0
    self._batch_size = batch_size
    self._warm_steps = warm_steps

  def begin(self):
    self._global_step_tensor = tf.train.get_global_step()
    if self._global_step_tensor is None:
      raise RuntimeError(
          'Global step should be created to use StepCounterHook.')

  def before_run(self, run_context):
    return tf.train.SessionRunArgs(self._global_step_tensor)

  def after_run(self, run_context, run_values):
    global_step = run_values.results

    if self._timer.should_trigger_for_step(
        global_step) and global_step > self._warm_steps:
      elapsed_time, elapsed_steps = self._timer.update_last_triggered_step(
          global_step)
      
      if elapsed_time is not None:
        self._step_train_time += elapsed_time
        self._total_steps += elapsed_steps
        average_examples_per_sec = self._batch_size * (
            self._total_steps / self._step_train_time)
        current_examples_per_sec = self._batch_size * (
            elapsed_steps / elapsed_time)
        tf.logging.info('Batch [%g]:  current exp/sec = %g, average exp/sec = '
                        '%g', self._total_steps, current_examples_per_sec,
                        average_examples_per_sec)

_TENSORS_TO_LOG = dict((x, x) for x in ['learning_rate',
                                        'cross_entropy',
                                        'train_accuracy'])

def get_train_hooks(name_list, **kwargs):
  if not name_list:
    return []
  train_hooks = []
  for name in name_list:
    hook_name = HOOKS.get(name.strip().lower())
    if hook_name is None:
      raise ValueError('Unrecognized training hook requested: {}'.format(name))
    else:
      train_hooks.append(hook_name(**kwargs))
  return train_hooks

def get_logging_tensor_hook(every_n_iter=100, tensors_to_log=None, **kwargs):
  if tensors_to_log is None:
    tensors_to_log = _TENSORS_TO_LOG

  return tf.train.LoggingTensorHook(
      tensors=tensors_to_log,
      every_n_iter=every_n_iter)

def get_profiler_hook(save_steps=1000, **kwargs):
  return tf.train.ProfilerHook(save_steps=save_steps)

def get_examples_per_second_hook(every_n_steps=100,
                                 batch_size=128,
                                 warm_steps=5,
                                 **kwargs):
  return ExamplesPerSecondHook(every_n_steps=every_n_steps,
                                     batch_size=batch_size,
                                     warm_steps=warm_steps)

HOOKS = {
    'loggingtensorhook': get_logging_tensor_hook,
    'profilerhook': get_profiler_hook,
    'examplespersecondhook': get_examples_per_second_hook,
}

def define_columns():
    _CSV_COLUMNS = [
    'item_id', 'first_clicked_month', 'first_clicked_weekday', 'first_clicked_hour',
    'last_clicked_month', 'last_clicked_weekday', 'last_clicked_hour', 'item_time', 'category_clean',
    'item_click_counts', 'click_sums', 'itemP', 'session_time', 'session_start_month',
    'session_start_weekday', 'session_start_hour', 'session_end_month', 'session_end_weekday',
    'session_end_hour', 'max_item_time', 'max_clicks', 'total_clicks', 'unique_items',
    'first_item_id', 'last_item_id', 'oneP', 'max_time_item_id', 'first_item_ind',
    'last_item_ind', 'max_time_item_ind', 'Buy'
    ]

    _CSV_COLUMN_DEFAULTS = [[''], [''], [''], [''], [''], [''], [''], [0.1], [''], [0],
                        [0], [0.1], [0.1], [''], [''], [''], [''], [''], [''], [0.1],
                        [0], [0], [0], [''], [''], [0.1], [''], [0], [0], [0], [0]]

    _BUFFER_SIZE = {'1': 4258492, '2': 1064624,}
    
    return _CSV_COLUMNS, _CSV_COLUMN_DEFAULTS, _BUFFER_SIZE

def build_model_columns():
  """Builds a set of wide and deep feature columns."""
  # Continuous columns
  item_time = tf.feature_column.numeric_column('item_time')
  item_click_counts = tf.feature_column.numeric_column('item_click_counts')
  click_sums = tf.feature_column.numeric_column('click_sums')
  session_time = tf.feature_column.numeric_column('session_time')
  max_item_time = tf.feature_column.numeric_column('max_item_time')
  max_clicks = tf.feature_column.numeric_column('max_clicks')
  total_clicks = tf.feature_column.numeric_column('total_clicks')
  unique_items = tf.feature_column.numeric_column('unique_items')
  itemP = tf.feature_column.numeric_column('itemP')
  first_item_ind = tf.feature_column.numeric_column('first_item_ind')
  last_item_ind = tf.feature_column.numeric_column('last_item_ind')
  max_time_item_ind = tf.feature_column.numeric_column('max_time_item_ind')
  oneP = tf.feature_column.numeric_column('oneP')

  first_clicked_month = tf.feature_column.categorical_column_with_vocabulary_list(
      'first_clicked_month', [
          '4', '5', '6', '7', '8', '9'])

  last_clicked_month = tf.feature_column.categorical_column_with_vocabulary_list(
      'last_clicked_month', [
          '4', '5', '6', '7', '8', '9'])   

  session_start_month = tf.feature_column.categorical_column_with_vocabulary_list(
      'session_start_month', [
          '4', '5', '6', '7', '8', '9'])

  session_end_month = tf.feature_column.categorical_column_with_vocabulary_list(
      'session_end_month', [
          '4', '5', '6', '7', '8', '9'])

  first_clicked_weekday = tf.feature_column.categorical_column_with_vocabulary_list(
      'first_clicked_weekday', [
          '0', '1', '2', '3', '4', '5', '6'])

  last_clicked_weekday = tf.feature_column.categorical_column_with_vocabulary_list(
      'last_clicked_weekday', [
          '0', '1', '2', '3', '4', '5', '6'])

  session_start_weekday = tf.feature_column.categorical_column_with_vocabulary_list(
      'session_start_weekday', [
          '0', '1', '2', '3', '4', '5', '6'])

  session_end_weekday = tf.feature_column.categorical_column_with_vocabulary_list(
      'session_end_weekday', [
          '0', '1', '2', '3', '4', '5', '6'])
    
  first_clicked_hour = tf.feature_column.categorical_column_with_vocabulary_list(
      'first_clicked_hour', [
          '0', '1', '2', '3', '4', '5', '6' , '7', '8', '9', '10', '11',
          '12', '13', '14', '15', '16', '17', '18' , '19', '20', '21', '22', '23',])

  last_clicked_hour = tf.feature_column.categorical_column_with_vocabulary_list(
      'last_clicked_hour', [
          '0', '1', '2', '3', '4', '5', '6' , '7', '8', '9', '10', '11',
          '12', '13', '14', '15', '16', '17', '18' , '19', '20', '21', '22', '23',])

  session_start_hour = tf.feature_column.categorical_column_with_vocabulary_list(
      'session_start_hour', [
          '0', '1', '2', '3', '4', '5', '6' , '7', '8', '9', '10', '11',
          '12', '13', '14', '15', '16', '17', '18' , '19', '20', '21', '22', '23',])

  session_end_hour = tf.feature_column.categorical_column_with_vocabulary_list(
      'session_end_hour', [
          '0', '1', '2', '3', '4', '5', '6' , '7', '8', '9', '10', '11',
          '12', '13', '14', '15', '16', '17', '18' , '19', '20', '21', '22', '23',])
    
  # Hashing:
  item_id = tf.feature_column.categorical_column_with_hash_bucket(
      'item_id', hash_bucket_size=66000)

  category_clean = tf.feature_column.categorical_column_with_hash_bucket(
      'category_clean', hash_bucket_size=20)
  
  first_item_id = tf.feature_column.categorical_column_with_hash_bucket(
      'first_item_id', hash_bucket_size=66000)
  
  last_item_id = tf.feature_column.categorical_column_with_hash_bucket(
      'last_item_id', hash_bucket_size=66000)
  
  max_time_item_id = tf.feature_column.categorical_column_with_hash_bucket(
      'max_time_item_id', hash_bucket_size=66000)

  # Wide columns and deep columns.
  base_columns = [
      first_clicked_month, first_clicked_weekday, first_clicked_hour,
      last_clicked_month, last_clicked_weekday, last_clicked_hour,
      session_start_month, session_start_weekday, session_start_hour,
      session_end_month, session_end_weekday, session_end_hour,
      item_id, category_clean, first_item_id, last_item_id, max_time_item_id
  ]

  crossed_columns = [
#      tf.feature_column.crossed_column(
#          ['first_clicked_month', 'last_clicked_month', 'first_clicked_hour',
#           'last_clicked_month', 'last_clicked_weekday', 'last_clicked_hour',
#           'session_start_month', 'session_start_weekday', 'session_start_hour',
#           'session_end_month', 'session_end_weekday', 'session_end_hour'], hash_bucket_size=5000),
      tf.feature_column.crossed_column(
          ['item_id', 'category_clean'], hash_bucket_size=66000),
      tf.feature_column.crossed_column(
          ['first_item_id', 'category_clean'], hash_bucket_size=66000),
      tf.feature_column.crossed_column(
          ['last_item_id', 'category_clean'], hash_bucket_size=66000),
      tf.feature_column.crossed_column(
          ['max_time_item_id', 'category_clean'], hash_bucket_size=66000)
  ]

  wide_columns = base_columns + crossed_columns
  
  deep_columns = [
        item_time,
        item_click_counts,
        click_sums,
        session_time,
        max_item_time,
        max_clicks,
        total_clicks,
        unique_items,
        itemP,
        oneP,
        first_item_ind,
        last_item_ind,
        max_time_item_ind,
        tf.feature_column.indicator_column(first_clicked_month),
        tf.feature_column.indicator_column(first_clicked_weekday),
        tf.feature_column.indicator_column(first_clicked_hour),
        tf.feature_column.indicator_column(last_clicked_month),
        tf.feature_column.indicator_column(last_clicked_weekday),
        tf.feature_column.indicator_column(last_clicked_hour),
        tf.feature_column.indicator_column(session_start_month),
        tf.feature_column.indicator_column(session_start_weekday),
        tf.feature_column.indicator_column(session_start_hour),
        tf.feature_column.indicator_column(session_end_month),
        tf.feature_column.indicator_column(session_end_weekday),
        tf.feature_column.indicator_column(session_end_hour),
      # Embedding
      tf.feature_column.embedding_column(item_id, dimension=16),
      tf.feature_column.embedding_column(category_clean, dimension=4),
      tf.feature_column.embedding_column(first_item_id, dimension=16),
      tf.feature_column.embedding_column(last_item_id, dimension=16),
      tf.feature_column.embedding_column(max_time_item_id, dimension=16),
  ]

  FNN_columns = [
        item_time,
        item_click_counts,
        click_sums,
        session_time,
        max_item_time,
        max_clicks,
        total_clicks,
        unique_items,
        itemP,
        oneP,
        first_item_ind,
        last_item_ind,
        max_time_item_ind,   
        tf.feature_column.embedding_column(first_clicked_month, dimension=8),
        tf.feature_column.embedding_column(first_clicked_weekday, dimension=8),        
        tf.feature_column.embedding_column(first_clicked_hour, dimension=8),        
        tf.feature_column.embedding_column(last_clicked_month, dimension=8),        
        tf.feature_column.embedding_column(last_clicked_weekday, dimension=8),        
        tf.feature_column.embedding_column(last_clicked_hour, dimension=8),        
        tf.feature_column.embedding_column(session_start_month, dimension=8),        
        tf.feature_column.embedding_column(session_start_weekday, dimension=8),        
        tf.feature_column.embedding_column(session_start_hour, dimension=8),        
        tf.feature_column.embedding_column(session_end_month, dimension=8),        
        tf.feature_column.embedding_column(session_end_weekday, dimension=8),
        tf.feature_column.embedding_column(session_end_hour, dimension=8),       
        tf.feature_column.embedding_column(item_id, dimension=8),
        tf.feature_column.embedding_column(category_clean, dimension=8),
        tf.feature_column.embedding_column(first_item_id, dimension=8),
        tf.feature_column.embedding_column(last_item_id, dimension=8),
        tf.feature_column.embedding_column(max_time_item_id, dimension=8),
  ]
  
  PNN_columns = deep_columns + crossed_columns
  
  return wide_columns, deep_columns, FNN_columns, PNN_columns