import pandas as pd
from sklearn.preprocessing import LabelEncoder


class MethodCode(object):
    MethodOnehot = 1
    MethodLabel = 2


class MethodEncoder(object):
    def __init__(self, df):
        self.df = df

    def encode(self, method_code, col_name):
        if method_code == MethodCode.MethodLabel:
            encoder = LabelEncoder()
            self.df[col_name] = encoder.fit_transform(self.df[col_name])
        elif method_code == MethodCode.MethodOnehot:
            self.df = pd.concat([self.df, pd.get_dummies(self.df[col_name], prefix=col_name)], axis=1)
            self.df.drop([col_name], axis=1, inplace=True)

    def get_result(self):
        return self.df
