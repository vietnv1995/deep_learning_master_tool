# ==============================================================================

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import os
import shutil
import sys

import tensorflow as tf
import matplotlib.pyplot as plt
import pandas as pd

import sub_modules

_CSV_COLUMNS, _CSV_COLUMN_DEFAULTS, _BUFFER_SIZE = sub_modules.define_columns()

class ModelArgParser(argparse.ArgumentParser):

  def __init__(self):
    super(ModelArgParser, self).__init__(parents=[sub_modules.BaseParser()])
    self.add_argument(
        '--model_type', '-mt', type=str, default='PNN',
        choices=['wide', 'deep', 'wide_deep', 'FNN', 'PNN'],
        help='[default %(default)s] Valid model types: wide, deep, wide_deep, FNN, PNN.',
        metavar='<MT>')
    self.set_defaults(
        data_dir='Data',
        model_dir='Model',
        train_epochs=1,
        epochs_between_evals=1,
        batch_size=64,
        model_type='wide_deep')
    
"""Tao mo hinh (su dung API cua tensorflow ver. >= 1.6, Optimizer mac dinh: Adagrad)"""
def build_estimator(model_dir, model_type):
  wide_columns, deep_columns, FNN_columns, PNN_columns = sub_modules.build_model_columns()
  hidden_units = [500, 400, 300] # Cau truc phan mang hoc sau. VD: [200,200] mang 2 lop an voi 200 no-ron moi lop; [100, 100, 100, 100]: mang 4 lop an voi 100 no-ron moi lop
  learning_rate = 0.05
  run_config = tf.estimator.RunConfig().replace(
     session_config=tf.ConfigProto(device_count={'GPU': 0}))
  
  if model_type == 'wide':
    return tf.estimator.LinearClassifier(
        model_dir=model_dir,
        feature_columns=wide_columns,
        config=run_config)
  elif model_type == 'deep':
    return tf.estimator.DNNClassifier(
        model_dir=model_dir,
        feature_columns=deep_columns,
        hidden_units=hidden_units,
        optimizer=tf.train.AdagradOptimizer(learning_rate=learning_rate),
        config=run_config)
  elif model_type == 'FNN':
    return tf.estimator.DNNClassifier(
        model_dir=model_dir,
        feature_columns=FNN_columns,
        hidden_units=hidden_units,
        optimizer=tf.train.AdagradOptimizer(learning_rate=learning_rate),
        config=run_config)
  elif model_type == 'PNN':
    return tf.estimator.DNNClassifier(
        model_dir=model_dir,
        feature_columns=PNN_columns,
        hidden_units=hidden_units,
        optimizer=tf.train.AdagradOptimizer(learning_rate=learning_rate),
        config=run_config)
  else:
    return tf.estimator.DNNLinearCombinedClassifier(
        model_dir=model_dir,
        linear_feature_columns=wide_columns,
        dnn_feature_columns=deep_columns,
        dnn_hidden_units=hidden_units,
        dnn_optimizer=tf.train.AdagradOptimizer(learning_rate=learning_rate),
        dnn_dropout=0.1,
        config=run_config)

"""Tao input"""
def input_fn(data_file, num_epochs, shuffle, batch_size):
  assert tf.gfile.Exists(data_file), ('%s khong ton tai.' % data_file)

  def parse_csv(value):
    print('Parsing', data_file)
    columns = tf.decode_csv(value, record_defaults=_CSV_COLUMN_DEFAULTS)
    features = dict(zip(_CSV_COLUMNS, columns))
    labels = features.pop('Buy')
    return features, tf.equal(labels, 1)

  dataset = tf.data.TextLineDataset(data_file)

  if shuffle:
    dataset = dataset.shuffle(buffer_size=_BUFFER_SIZE['1'])

  dataset = dataset.map(parse_csv, num_parallel_calls=5)
  dataset = dataset.repeat(num_epochs)
  dataset = dataset.batch(batch_size)
  return dataset

"""Chay va xuat ket qua"""
def main(argv):
  parser = ModelArgParser()
  flags = parser.parse_args(args=argv[1:])
  shutil.rmtree(flags.model_dir, ignore_errors=True)
  model = build_estimator(flags.model_dir, flags.model_type)
  Index = pd.DataFrame(index = range(flags.train_epochs // flags.epochs_between_evals), columns = ['AUC', 'Loss', 'Accuracy'])

  train_file = os.path.join(flags.data_dir, 'train.dat')
  test_file = os.path.join(flags.data_dir, 'test.dat')
  
  def train_input_fn():
    return input_fn(train_file, flags.epochs_between_evals, True, flags.batch_size)

  def eval_input_fn():
    return input_fn(test_file, 1, False, flags.batch_size)

  train_hooks = sub_modules.get_train_hooks(
      flags.hooks, batch_size=flags.batch_size,
      tensors_to_log={'average_loss': 'head/truediv',
                      'loss': 'head/weighted_loss/Sum'})
  
  for n in range(flags.train_epochs // flags.epochs_between_evals):
    model.train(input_fn=train_input_fn, hooks=train_hooks)
    results = model.evaluate(input_fn=eval_input_fn)

    print('Results at epoch', (n + 1) * flags.epochs_between_evals)
    print('-' * 60)

    for key in sorted(results):
      print('%s: %s' % (key, results[key]))

    Index.loc[n, 'AUC'] = results.get('auc',)
    Index.loc[n, 'Loss'] = results.get('average_loss',)
    Index.loc[n, 'Accuracy'] = results.get('accuracy',)
    print(Index)
    Index.to_csv('Index_item.dat')
  Index.AUC.plot()
  plt.ylabel('AUC')
  plt.xlabel('Epoch')
  plt.show()
  Index.Loss.plot()
  plt.ylabel('Loss')
  plt.xlabel('Epoch')
  plt.show()
  Index.Accuracy.plot()
  plt.ylabel('Accuracy')
  plt.xlabel('Epoch')
  plt.show()
  
if __name__ == '__main__':
  tf.logging.set_verbosity(tf.logging.INFO)
  main(argv=sys.argv)

#  tensorboard --logdir="E:\Self_Learning\ANN_n_Python\Test_Wide_n_Deep\TensorFlow\Model"