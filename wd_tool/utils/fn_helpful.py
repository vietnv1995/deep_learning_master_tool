
def change_dict_to_df(dic):
    d_res = {}
    for prefix, vals in dic.items():
        if prefix in ['train', 'test']:
            for k, v in vals.items():
                d_res[str(prefix) + "_" + str(k)] = v
        else:
            d_res[prefix] = vals
    return d_res