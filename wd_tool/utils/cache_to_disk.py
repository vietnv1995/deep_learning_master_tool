import pickle
import os

class CacheToDisk(object):
    def __init__(self, base_path):
        self.base_path = base_path
        self.create_folder(base_path)

    def create_folder(self, path):
        if not os.path.exists(path):
            os.makedirs(path)

    def set_cache(self, file_name, obj):
        with open(os.path.join(self.base_path, file_name), 'wb') as f:
            pickle.dump(obj, f)

    def get_cache(self, file_name):
        try:
            with open(os.path.join(self.base_path, file_name), 'rb') as f:
                obj = pickle.load(f)
            return obj
        except:
            return None