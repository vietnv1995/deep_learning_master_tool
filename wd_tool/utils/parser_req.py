from wd_tool.core.fill_nan_method import FillNanCode
import itertools

def parse_req_fillna(request):
    col_types = {}
    col_types_name = {}
    col_fillnas = {}
    col_cats = []
    for key, value in request.POST.items():
        # key example tb1_type_REGION_POPULATION_RELATIVE or tb3_nan_OWN_CAR_AGE or
        if key[4:8] == "type":
            col_name = key[9:]
            col_types[col_name] = make_type(value)
            col_types_name[col_name] = value
        elif key[4:7] == 'nan':
            col_name = key[8:]
            col_fillnas[col_name] = make_type_na(value)
        elif key[4:6] == 'is':
            col_name = key[7:]
            col_cats.append(col_name)
    for col_cat in col_cats:
        col_types[col_cat] = make_type("string")
        col_types_name[col_cat] = "string"
    return col_fillnas, col_types, col_cats, col_types_name

def make_type(type_name):
    if type_name == 'int':
        return [0]
    elif type_name == "float":
        return [0.1]
    else:
        return ['']

def make_type_na(type_name):
    if type_name == 'mean':
        return FillNanCode.FillMean
    elif type_name == 'mode':
        return FillNanCode.FillMode
    elif type_name == 'zero':
        return FillNanCode.FillZero

def parse_req_model_config(request):
    # Example req: [tb2_deep_FLOORSMAX_MODE, tb3_wide_FLAG_DOCUMENT_14:10 x FLAG_MOBIL:10,
    # tb3_dimension_hash_FLAG_DOCUMENT_14:100 x FLAG_MOBIL:100] [{k: tb2_DEF_60_CNT_SOCIAL_CIRCLE, val: tb2_DEF_60_CNT_SOCIAL_CIRCLE}]
    wide_cols = []
    deep_cols = []
    ignore_cols = []
    onehot_cols = []
    cross_cols = {}
    hash_cols = {}
    emb_cols = {}
    tmp_hash_cols = {}
    tmp_emb_cols = {}
    key_ignores = [
        'tb1_wide_choose_all', 'tb1_deep_choose_all', 'tb1_ignore_choose_all',
        'tb2_wide_choose_all', 'tb2_deep_choose_all', 'tb2_ignore_choose_all',
        'tb3_wide_choose_all', 'tb3_deep_choose_all', 'tb3_ignore_choose_all',
        'tb2_emball', 'tb2_emb_choose_all', 'tb2_hashball', 'tb2_choose_all',
                       ]
    for k, v in request.POST.items():
        if k in key_ignores:
            continue
        if k[0:6] == 'encode':
            if v[0:6] == 'onehot':
                onehot_cols.append(k[7:])
            else:
                hash_cols[k[7:]] = 1000
        elif k[4:8]=='wide':
            wide_cols.append(k[9:])
        elif k[4:8] == 'deep':
            deep_cols.append(k[9:])
        elif k[4:10] == 'ignore':
            ignore_cols.append(k[11:])
        elif k.startswith("tb2_dimension_hash") and v != "":
            tmp_hash_cols[k[19:]] = int(v)
        elif k.startswith("tb2_dimension_emb") and v != "":
            tmp_emb_cols[k[18:]] = int(v)
        elif k.startswith("tb3_dimension_hash") and v != "":
            cross_cols[k[19:]] = int(v)
        elif k.startswith("tb2_emb"):
            emb_cols[k[8:]] = 100
    emb_cols = {k: tmp_emb_cols.get(k, 100) for k, v in emb_cols.items()}
    hash_cols = {k: tmp_hash_cols.get(k, 1000) for k, v in hash_cols.items()}
    # print("Wide {}".format(wide_cols))
    # print("Deep {}".format(deep_cols))
    # print("Ignore {}".format(ignore_cols))
    # print("Onehot {}".format(onehot_cols))
    # print("Hash {}".format(hash_cols))
    # print("Cross {}".format(cross_cols))
    # print("Embedding {}".format(emb_cols))
    layers = request.POST.get("layers")
    layers = parse_layers(layers)
    dropouts = request.POST.get("dropouts")
    dropouts = [float(item) for item in dropouts.split("x")]
    if len(dropouts) < len(layers):
        for i in range(len(layers) - len(dropouts)):
            dropouts.append(0)
    elif len(dropouts) > len(layers):
        dropouts = dropouts[0: len(layers)]

    loss_function = request.POST.get("loss_function")
    epochs = int(request.POST.get("epochs"))

    optimizer = request.POST.get("optimizer", "adam")
    activation = request.POST.get("activation", "relu")
    test_size = float(request.POST.get("test_size"))
    shuffle = int(request.POST.get("shuffle"))
    metrics = request.POST.getlist("metrics")
    model_types = request.POST.getlist("model_type")
    if len(wide_cols) == 0:
        if 'wide' in model_types:
            model_types.remove('wide')
        if 'wd' in model_types:
            model_types.remove('wd')
    if len(deep_cols) == 0:
        if 'deep' in model_types:
            model_types.remove('deep')
            model_types.remove('wd')
    print(model_types)
    model_config = {'layers': layers,
                    'dropouts': dropouts,
                    'loss_function': loss_function,
                    'epochs': epochs,
                    'optimizer': optimizer,
                    'activation': activation,
                    'test_size': test_size,
                    'shuffle': shuffle,
                    'metrics': metrics,
                    'model_type': model_types}
    config_columns = {'wide_cols': wide_cols,
                      'deep_cols': deep_cols,
                      'ignore_cols': ignore_cols,
                      'onehot_cols': onehot_cols,
                      'cross_cols': cross_cols,
                      'hash_cols': hash_cols,
                      'emb_cols': emb_cols}
    return config_columns, model_config

def parse_layers(layers):
    res = []
    for layer in layers.split("x"):
        layer_split = layer.split("-")
        if len(layer_split) == 1:
            res.append([int(layer_split[0])])
        elif len(layer_split) == 2:
            res.append([num for num in range(int(layer_split[0]), int(layer_split[1]))])
    list_layer_trys = list(itertools.product(*res))
    return list_layer_trys