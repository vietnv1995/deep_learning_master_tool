from django.shortcuts import render, redirect
import os
import pandas as pd
import numpy as np
from django.http import JsonResponse
from dl_tool.core.const import ROUND
from pandas.api.types import is_string_dtype
from pandas.api.types import is_numeric_dtype
import itertools
import json
import time
from wd_tool.utils.cache_to_disk import CacheToDisk
from wd_tool.utils.parser_req import parse_req_fillna, parse_req_model_config
from wd_tool.utils.fn_helpful import change_dict_to_df
from wd_tool.core.fill_nan_method import FillNanMethod
from wd_tool.core.data_table import DataTable
from wd_tool.core.wide_deep_model import WideDeepModel
from dl_tool.utils.fn_helpful import round_df
import dask.dataframe as dd


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

BASE_DIR_DATA = os.path.join(*[BASE_DIR, "wd_tool", "data_upload"])
BASE_DIR_FULL = os.path.join(*[BASE_DIR, "wd_tool", "data_full"])
BASE_DIR_RESULT = os.path.join(*[BASE_DIR, "wd_tool", "results"])
BASE_DIR_CONFIG = os.path.join(*[BASE_DIR, "wd_tool", "config_log"])
BASE_DIR_MODEL = os.path.join(*[BASE_DIR, "wd_tool", "model_dir"])

BASE_DIR_CACHE = os.path.join(*[BASE_DIR, "wd_tool", "cache"])
Cacher = CacheToDisk(BASE_DIR_CACHE)
CACHE = True

def init():
    create_folder(BASE_DIR_DATA)
    create_folder(BASE_DIR_FULL)
    create_folder(BASE_DIR_RESULT)
    create_folder(BASE_DIR_CONFIG)
    create_folder(BASE_DIR_MODEL)

def create_folder(path):
    if not os.path.exists(path):
        os.makedirs(path)

current_file = ()
init()

def convert_test_file_to_csv(file_name):
    try:
        df = pd.read_csv(os.path.join(BASE_DIR_DATA, file_name), nrows=5)
    except Exception as e:
        df = pd.read_excel(os.path.join(BASE_DIR_DATA, file_name), nrows=5)
        df_tmp = pd.read_excel(os.path.join(BASE_DIR_DATA, file_name))
        file_name = file_name.split('.')[0] + '.csv'
        df_tmp.to_csv(os.path.join(BASE_DIR_DATA, file_name), index=False)
    return file_name

# Create your views here.
def upload(request):
    if request.method == 'GET':
        return render(request, 'wd_upload.html')
    else:
        train_name = request.FILES['train'].name
        handle_uploaded_file(request.FILES['train'], train_name)
        test_name = ""
        if request.FILES.get("test", None) is not None:
            test_name = request.FILES.get('test').name
            handle_uploaded_file(request.FILES['test'], test_name)
            test_name = convert_test_file_to_csv(test_name)
        try:
            df = pd.read_csv(os.path.join(BASE_DIR_DATA, train_name), nrows=5)
        except Exception as e:
            df = pd.read_excel(os.path.join(BASE_DIR_DATA, train_name), nrows=5)
            df_tmp = pd.read_excel(os.path.join(BASE_DIR_DATA, train_name))
            train_name = train_name.split('.')[0] + '.csv'
            df_tmp.to_csv(os.path.join(BASE_DIR_DATA, train_name), index=False)
        print(df.head())
        cols = ["Column Name", "Type", "Ex1", "Ex2", "Ex3", "Ex4", "Ex5", "Is Target"]
        '''
        [{'col': col_name, 'type': type, 'values': [values]}]
        '''
        res = []
        for col in df.columns:
            col_type = None
            if is_numeric_dtype(df[col]):
                col_type = 'number'
            elif is_string_dtype(df[col]):
                col_type = 'string'
            d = {'col': col, 'type': col_type, 'values': df[col].tolist()}
            res.append(d)
        return render(request, "wd_preview_data.html", {'records': res, 'train': train_name, 'test': test_name, 'cols': cols, "test_name": test_name})

def handle_uploaded_file(f, name):
    global CACHE
    CACHE = False
    with open(os.path.join(BASE_DIR_DATA, name), 'wb') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

def calculate_summary(file_name, target_name, max_unique):
    global CACHE
    file_name_cache = "{}_{}_{}_summary_cache.pickle".format(file_name, target_name, max_unique)
    if CACHE:
        cache_obj = Cacher.get_cache(file_name_cache)
        if cache_obj is not None:
            return cache_obj['results'], cache_obj['cols'], cache_obj['summary_totals']
    df = dd.read_csv(os.path.join(BASE_DIR_DATA, file_name))
    df = df.compute()
    df.rename(columns={target_name: 'TARGET'}, inplace=True)
    df.to_csv(os.path.join(BASE_DIR_DATA, file_name), index=False)
    print("Rename file")
    df.drop(['TARGET'], axis=1, inplace=True)
    df.replace([np.inf, -np.inf], np.nan, inplace=True)
    n_rows = df.shape[0]
    res_number_not_null = []
    res_number_is_null = []
    res_string_not_null = []
    res_string_is_null = []
    columns = list(df.columns)
    columns.sort()

    for col in columns:
        n_nans = df[col].isna().sum()
        n_uniques = df[col].nunique()
        d = {'col': col, 'n_nans': n_nans, 'n_uniques': n_uniques}

        if df[col].dtype == np.int64:
            col_type = 'int'
            d['type'] = col_type
            if n_uniques <= min(max_unique, n_rows/3):
                d['n_uniques'] = n_uniques
                if n_nans == 0:
                    res_string_not_null.append(d)
                else:
                    res_string_is_null.append(d)
                continue
            if n_nans == 0:
                res_number_not_null.append(d)
            else:
                res_number_is_null.append(d)
        elif df[col].dtype == np.float64:
            col_type = 'float'
            d['type'] = col_type
            if n_uniques <= min(max_unique, n_rows/3):
                d['n_uniques'] = n_uniques
                if n_nans == 0:
                    res_string_not_null.append(d)
                else:
                    res_string_is_null.append(d)
                continue
            if n_nans == 0:
                res_number_not_null.append(d)
            else:
                res_number_is_null.append(d)
        else:
            col_type = 'string'
            d['type'] = col_type
            if n_nans == 0:
                res_string_not_null.append(d)
            else:
                res_string_is_null.append(d)
    results = {'res_number_not_null': res_number_not_null, 'res_number_is_null': res_number_is_null,
               'res_string_not_null': res_string_not_null, 'res_string_is_null': res_string_is_null}
    cols = {'col_num_not_null': "|".join([item.get("col") for item in res_number_not_null]),
            'col_num_is_null': "|".join([item.get("col") for item in res_number_is_null]),
            'col_str_not_null': "|".join([item.get("col") for item in res_string_not_null]),
            'col_str_is_null': "|".join([item.get("col") for item in res_string_is_null]),
            }

    summary_totals = {'n_rows': n_rows, 'n_cols': len(df.columns),
                      'n_col_num_not_null': len(res_number_not_null), 'n_col_num_is_null': len(res_number_is_null),
                      'n_col_str_not_null': len(res_string_not_null), 'n_col_str_is_null': len(res_string_is_null)}
    CACHE = True
    Cacher.set_cache(file_name_cache, {'results': results, 'cols': cols, 'summary_totals': summary_totals})
    return results, cols, summary_totals

def save_file_fillna(current_file, col_fillnas):
    src_file_train = current_file[0]
    tgr_file_train = current_file[0][0:-4] + "_fillna.csv"
    df_train = dd.read_csv(src_file_train)
    df_train = df_train.compute()
    df_test = None
    tgr_file_test = ""
    if current_file[1] != "":
        src_file_test = current_file[1]
        tgr_file_test = current_file[1][0:-4] + "_fillna.csv"
        df_test = dd.read_csv(src_file_test)
        df_test = df_test.compute()
    fillna_obj = FillNanMethod(df_train, df_test)
    fillna_obj.fillnans(col_fillnas)
    df_train, df_test = fillna_obj.get_result()
    df_train.to_csv(tgr_file_train, index=False)
    if df_test is not None:
        df_test.to_csv(tgr_file_test, index=False)
    return tgr_file_train, tgr_file_test

def data_quanlity(request):
    global current_file
    if request.method == 'GET':
        train_file = request.GET['train']
        test_file = request.GET['test']
        target_name = request.GET['target_name']
        n_unique = int(request.GET.get('n_unique', 50))
        if test_file != "":
            current_file = (os.path.join(BASE_DIR_DATA, train_file), os.path.join(BASE_DIR_DATA, test_file))
            df = dd.read_csv(current_file[1])
            df = df.compute()
            df.rename(columns={target_name: 'TARGET'}, inplace=True)
            df.to_csv(current_file[1], index=False)
        else:
            current_file = (os.path.join(BASE_DIR_DATA, train_file), "")
        # cols = ["Column Name", "Type", "Null Count", "Ex2", "Ex3", "Ex4", "Ex5"]
        '''
        [{'col': col_name, 'type': type, 'n_nans': n_nans}]
        '''
        results, cols, summary_totals = calculate_summary(train_file, target_name, n_unique)
        return render(request, "wd_summary_data.html", {'results': results, 'file_name': train_file,
                                                     'cols': cols, 'summary_totals': summary_totals})
    else:
        col_fillnas, col_types, col_cats, col_types_name = parse_req_fillna(request)
        if len(col_fillnas) > 0:
            tgr_file_train, tgr_file_test = save_file_fillna(current_file, col_fillnas)
            current_file = (tgr_file_train, tgr_file_test)
        df = dd.read_csv(current_file[0])
        df = df.compute()
        uv = df.nunique().to_dict()
        test_table = None
        train_table = DataTable(current_file[0], col_types, col_cats, col_types_name, uv)
        tr_table_sample = make_data_table_sample(df, col_types, col_cats, col_types_name, True)
        t_table_sample = None
        if current_file[1] != "":
            df_test = dd.read_csv(current_file[1])
            df_test = df_test.compute()
            uv = df_test.nunique().to_dict()
            test_table = DataTable(current_file[1], col_types, col_cats, col_types_name, uv)
            t_table_sample = make_data_table_sample(df_test, col_types, col_cats, col_types_name, False)
        Cacher.set_cache("sample_data_set.pickle", (tr_table_sample, t_table_sample))
        Cacher.set_cache("data_set.pickle", (train_table, test_table))
        return redirect(feature_selection)

def make_data_table_sample(df, col_types, col_cats, col_types_name, is_train):
    df_sample = df.sample(min(1000, df.shape[0]))
    if is_train:
        df_sample.to_csv(os.path.join(BASE_DIR_DATA, "train_sample_data.csv"), index=False)
        uv_sample = df_sample.nunique().to_dict()
        table_sample = DataTable(os.path.join(BASE_DIR_DATA, "train_sample_data.csv"), col_types, col_cats, col_types_name, uv_sample)
    else:
        df_sample.to_csv(os.path.join(BASE_DIR_DATA, "test_sample_data.csv"), index=False)
        uv_sample = df_sample.nunique().to_dict()
        table_sample = DataTable(os.path.join(BASE_DIR_DATA, "test_sample_data.csv"), col_types, col_cats, col_types_name, uv_sample)
    return table_sample


def feature_selection(request):
    dataset = Cacher.get_cache("data_set.pickle")
    sample_dataset = Cacher.get_cache("sample_data_set.pickle")
    if request.method == 'GET':
        train_table = dataset[0]
        cols_numberic = train_table.get_numberic_columns()
        cols_categorical = train_table.get_categorical_columns()
        cols_cross = train_table.get_cross_columns()
        print(cols_categorical[0:5])
        return render(request, "wd_config_model.html",
                      context={
                          'cols_numberic': cols_numberic,
                          'cols_categorical': cols_categorical,
                          'cols_cross': {"key_pair": " , ".join([item["name"] for item in cols_cross]),
                                         "hash_vals": "x".join([str(item["hash_size"]) for item in cols_cross])
                                         },
                          'cross_col_with_uv': ["{}:{}".format(item['name'], item['n_unique']) for item in
                                                cols_categorical],
                          'cols': {'numberic': "|".join([item['name'] for item in cols_numberic]),
                                    'categorical': "|".join([item['name'] for item in cols_categorical]),
                                   'cross': "|".join([item['name'] for item in cols_cross])}})
    else:
        config_columns, model_config = parse_req_model_config(request)
        dataset[0].set_config_columns(config_columns)
        if dataset[1] is not None:
            dataset[1].set_config_columns(config_columns)
        num_cols_info = dataset[0].get_numberic_columns()
        cate_cols_info = dataset[0].get_categorical_columns()
        all_cols = []
        num_cols = []
        for num_col in num_cols_info:
            all_cols.append(num_col.get("name"))
            num_cols.append(num_col.get("name"))
        for cate_col in cate_cols_info:
            all_cols.append(cate_col.get("name"))
        config_columns["all_cols"] = all_cols
        config_columns["num_cols"] = num_cols
        Cacher.set_cache("data_set.pickle", dataset)
        sample_dataset[0].set_config_columns(config_columns)
        sample_dataset[1].set_config_columns(config_columns)
        Cacher.set_cache("sample_data_set.pickle", sample_dataset)
        # Check is run auto model?
        # If yes, get sample data and run auto model
        # Else, check run kfold or no
        layers = model_config.get('layers')
        auto_mode = True if len(layers) > 1 else False
        if auto_mode:
            layers = model_config.get('layers')
            deep_res = []
            for layer in layers:
                model_config['layers'] = [layer]
                wd_obj = WideDeepModel(sample_dataset[0], sample_dataset[1], model_config, BASE_DIR_MODEL)
                res = run_normal(request, wd_obj, model_config, config_columns, auto_mode, dataset)
                out_dict = parse_result(res)
                out_dict['layer'] = layer
                deep_res.append(out_dict)
            df_deep = pd.DataFrame(deep_res)
            cols, records, file_result_name = make_result_auto_to_show_table(df_deep)
            cols_use = make_cols_use(config_columns)
            return render(request, 'wd_result.html', context={'cols': cols, 'records': records,
                                                              'cols_use': cols_use,
                                                              "transaction_id": file_result_name})
        else:
            wd_obj = WideDeepModel(dataset[0], dataset[1], model_config, BASE_DIR_MODEL)
            test_size = model_config.get("test_size")
            if test_size > 1:
                return run_kfold(request, wd_obj, test_size, model_config, config_columns, dataset)
            else:
                return run_normal(request, wd_obj, model_config, config_columns, auto_mode, dataset)


def make_result_auto_to_show_table(df_deep):
    df_deep.sort_values(by="t_acc", inplace=True, ascending=False)
    max_acc = df_deep.iloc[0]['t_acc']
    max_auc = df_deep.iloc[0]['t_auc']
    suffix_name = str(int(time.time()))
    max_layer = df_deep.iloc[0]['layer']
    file_result_name = "{}_{}_{}_auto.{}".format(max_acc, max_auc, suffix_name, max_layer)
    df_deep.to_csv(os.path.join(BASE_DIR_RESULT, file_result_name), index=False)
    cols = list(df_deep.columns)
    records = list(df_deep[cols].values)
    return cols, records, file_result_name


def parse_result(res):
    out_dict = {}
    t_dict = res.get("deep", {}).get("t", {})
    for metric, value in t_dict.items():
        out_dict["%s_%s"%("t", metric)] = value
    tr_dict = res.get("deep", {}).get("tr", {})
    for metric, value in tr_dict.items():
        out_dict["%s_%s"%("tr", metric)] = value
    return out_dict

def run_kfold(request, wd_obj, test_size, model_config, config_columns, dataset):
    fold_res = wd_obj.fit_with_kfold(int(test_size), model_config)
    data_show, data_summary_show, data_final_result, suffix_file_name \
        = save_res_kfold(fold_res, model_config, dataset)
    cols_use = make_cols_use(config_columns)
    model_info = get_info_model(model_config)
    return render(request, 'wd_result.html',
                  context={"data_show": data_show,
                           "data_summary_show": data_summary_show,
                           "data_final_show": data_final_result,
                           "transaction_id": suffix_file_name,
                           "cols_use": cols_use,
                           "model_info": model_info})

def run_normal(request, wd_obj, model_config, config_columns, auto_mode, dataset):
    if auto_mode:
        res = wd_obj.fit(model_config)
        return res
    else:
        res = wd_obj.fit(model_config)
        col_show, rs_show, suffix_name = save_res(res, model_config, config_columns, dataset)
        cols_use = make_cols_use(config_columns)
        model_info = get_info_model(model_config)
        return render(request, 'wd_result.html', context={'cols': col_show, 'records': rs_show,
                                                          'cols_use': cols_use,
                                                          "transaction_id": suffix_name,
                                                          "model_info": model_info})

def make_cols_use(config_columns):
    wide_cols = config_columns.get("wide_cols", [])
    deep_cols = config_columns.get("deep_cols", [])
    wide_cols.sort()
    deep_cols.sort()
    cols = ['Wide', "Deep"]
    rows = []
    for i in range(max(len(wide_cols), len(deep_cols))):
        row = []
        if i < len(wide_cols):
            col_info = get_info_col(config_columns, wide_cols[i])
            row.append("%s (%s)" % (wide_cols[i], col_info))
        else:
            row.append("")
        if i < len(deep_cols):
            col_info = get_info_col(config_columns, deep_cols[i])
            row.append("%s (%s)" % (deep_cols[i], col_info))
        else:
            row.append("")
        rows.append(row)
    return {'cols': cols, 'rows': rows}

def get_info_col(config_columns, column_name):
    col_info = ""
    if column_name in config_columns['onehot_cols']:
        col_info = col_info + ' - onehot'
    if column_name in config_columns['hash_cols'].keys():
        col_info = col_info + " - hash/%s" % config_columns['hash_cols'][column_name]
    if column_name in config_columns['emb_cols'].keys():
        col_info = col_info + " - emb/%s" % config_columns['emb_cols'].get(column_name)
    if column_name in config_columns['cross_cols'].keys():
        col_info = col_info + " - cross/%s" % config_columns["cross_cols"].get(column_name)
    return col_info

def get_info_model(model_config):
    layers = model_config.get("layers")[0]
    epochs = model_config.get("epochs", 1)
    fold = model_config.get("test_size")
    activation = model_config.get("activation")
    optimizer = model_config.get("optimizer")
    model_info = "Layer: %s ======= Epochs: %s ======= Fold: %s ====== Activation: %s ======= Optimizer: %s" % (layers, epochs, fold, activation, optimizer)
    return model_info

def read_result(path, diff_cols):
    # diff_cols = ['optimizer', 'activation', 'epochs', 'test_percent_0',
    #              'test_percent_1', 'train_percent_0', 'train_percent_1']
    df_res = pd.read_csv(path)
    if len(diff_cols) != 0:
        df_res = df_res[df_res.columns.difference(diff_cols)]
        first_column = 'fold'
        if first_column in df_res.columns:
            df_res = df_res.reindex(columns=([first_column] + list([a for a in df_res.columns if a != first_column])))
    return {'cols': list(df_res.columns), 'records': df_res.values.tolist(), 'file_name': path}

def save_config(suffix_name, all_config):
    column_config, model_config = convert_config(all_config)
    with open(os.path.join(BASE_DIR_CONFIG, suffix_name + "_col_config"), 'w') as f:
        json.dump(column_config, f)
    with open(os.path.join(BASE_DIR_CONFIG, suffix_name + "_model_config"), 'w') as f:
        json.dump(model_config, f)

def convert_config(all_config):
    column_config = all_config.get("column_config").copy()
    model_config = all_config.get("model_config")
    d = {}
    if "onehot_cols" in column_config.keys():
        for one_hot_col in column_config.get("onehot_cols"):
            d[one_hot_col] = "one_hot"
        column_config.pop("onehot_cols")
    if "hash_cols" in column_config.keys():
        for col, hash_size in column_config.get("hash_cols").items():
            d[col] = str(hash_size)
        column_config.pop("hash_cols")
    if "emb_cols" in column_config.keys():
        for col, emb_size in column_config.get("emb_cols").items():
            if col in d:
                d[col] = "{}-{}".format(d[col], emb_size)
        column_config.pop("emb_cols")
    column_config["feature_cols"] = d
    return column_config, model_config

def save_res(res, model_config, config_columns, dataset):
    #{'accuracy': 1.0, 'accuracy_baseline': 1.0, 'auc': 0.99999905,
    # 'auc_precision_recall': 0.0, 'average_loss': 0.0, 'label/mean': 0.0,
    # 'loss': 0.0, 'precision': 0.0, 'prediction/mean': 0.0, 'recall': 0.0, 'global_step': 16}
    timestamp_name = str(int(time.time()))
    suffix_file_name = "{}_{}".format(dataset[0].path_to_file.split("/")[-1][0:-4], timestamp_name)
    save_config(suffix_file_name, {"model_config": model_config, "column_config": config_columns})
    layer = model_config['layers'][0]
    layer = "x".join([str(unit) for unit in layer])
    rs_show = []
    items = []
    ignore_cols = ['tr_percent_1', 't_percent_1']
    col_show = []
    for name_model, results in res.items():
        col_show = ['model_type']
        row = [name_model]
        item = {'model_type': name_model}
        for type in ['t', 'tr']:
            res_metrics = results.get(type, {})
            for metric, metric_val in res_metrics.items():
                key = "{}_{}".format(type, metric)
                try:
                    item[key] = round(float(metric_val), ROUND)
                except:
                    item[key] = metric_val
                if key not in ignore_cols:
                    col_show.append(key)
                    row.append(item[key])
        items.append(item)
        file_name = "{}_{}_{}_{}_{}.{}".format(dataset[0].path_to_file.split("/")[-1][0:-4], item.get('t_acc'), item.get('t_auc'), name_model, timestamp_name, layer)
        df = pd.DataFrame([item])
        df.to_csv(os.path.join(BASE_DIR_RESULT, file_name), index=False)
        rs_show.append(row)
    return col_show, rs_show, suffix_file_name

def save_res_kfold(fold_res, model_config, dataset):
    layer = model_config['layers'][0]
    layer = "x".join([str(unit) for unit in layer])

    data_df = []
    list_model_type = model_config.get("model_type")
    for fold, res in enumerate(fold_res):
        for name_model, results in res.items():
            data_item = {'fold': fold, 'layer': layer}
            data_item['model_type'] = name_model

            for type in ['t', 'tr']:
                res_metrics = results.get(type, {})
                for metric, metric_val in res_metrics.items():
                    key = "{}_{}".format(type, metric)
                    try:
                        data_item[key] = round(float(metric_val), ROUND)
                    except:
                        data_item[key] = metric_val
            data_df.append(data_item)
    data_show = {}
    data_summary_show = {}
    data_final_result = {}
    record_final = []
    df = pd.DataFrame(data_df)
    timestamp_name = str(int(time.time()))
    suffix_file_name = "{}_{}".format(dataset[0].path_to_file.split("/")[-1][0:-4],timestamp_name)
    save_config(suffix_file_name, model_config)
    ignore_cols = ['tr_percent_1', 't_percent_1', 'fold', 'layer', 'model_type', 't_confus', 'tr_confus']
    ignore_cols_display = ['tr_percent_1', 't_percent_1']
    for model_type in list_model_type:
        df_model = df.loc[df['model_type']==model_type, :]
        columns = [col for col in df_model.columns if col not in ignore_cols]
        df_sum = df_model[columns].describe()
        df_sum.drop('count', inplace=True)
        df_sum.rename({'25%': 'Q1', '50%': 'median', '75%': 'Q3'}, inplace=True)
        df_sum = df_sum.reset_index()
        df_sum = round_df(df_sum)
        df_sum.to_csv(
            os.path.join(BASE_DIR_RESULT, "{}_{}_summary.csv".format(model_type, suffix_file_name)), index=False
        )
        mean_auc = df_sum.iloc[0]['t_auc']
        mean_acc = df_sum.iloc[0]['t_acc']
        file_name = "{}_{}_{}_{}_{}.{}".format(dataset[0].path_to_file.split("/")[-1][0:-4], model_type, mean_acc, mean_auc, timestamp_name, layer)
        df_model.to_csv(os.path.join(BASE_DIR_RESULT, file_name), index=False)
        cols = [col for col in df_model.columns if col not in ignore_cols_display]
        records = list(df_model[cols].values)
        data_show[model_type] = {"cols": cols, "records": records}
        data_summary_show[model_type] = {"cols": list(df_sum.columns), 'records': list(df_sum.values)}
        col_get_value = ['t_acc', 't_auc', 'tr_acc', 'tr_auc']
        record = [df_sum.iloc[0][col] for col in col_get_value]
        record.insert(0, model_type)
        record_final.append(record)
    data_final_result['cols'] = ['model_type', 't_acc', 't_auc', 'tr_acc', 'tr_auc']
    data_final_result['records'] = record_final
    return data_show, data_summary_show, data_final_result, suffix_file_name



