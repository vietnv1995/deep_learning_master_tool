from django.urls import path
from . import views

urlpatterns = [
    path('', views.upload, name='wd_upload'),
    path('preview/', views.data_quanlity, name='wd_data_quanlity'),
    path('data-quanlity/', views.data_quanlity, name='wd_data_quanlity'),
    path('feature-selection', views.feature_selection, name='wd_feature_selection')
]