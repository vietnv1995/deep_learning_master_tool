from django.apps import AppConfig


class WdToolConfig(AppConfig):
    name = 'wd_tool'
