from django.shortcuts import render
import os
import pandas as pd
import numpy as np
from django.http import JsonResponse
from dl_tool.core.manager_data_flow import ManagerDataFlow
from pandas.api.types import is_string_dtype
from pandas.api.types import is_numeric_dtype
from functools import lru_cache
import itertools
import datetime
import json
import time
from dl_tool.utils.fn_helpful import round_df

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

BASE_DIR_DATA = os.path.join(*[BASE_DIR, "dl_tool", "data_upload"])
BASE_DIR_RESULT = os.path.join(*[BASE_DIR, "dl_tool", "results"])
BASE_DIR_CONFIG = os.path.join(*[BASE_DIR, "dl_tool", "config_log"])

def init():
    create_folder(BASE_DIR_DATA)
    create_folder(BASE_DIR_RESULT)
    create_folder(BASE_DIR_CONFIG)

def create_folder(path):
    if not os.path.exists(path):
        os.makedirs(path)

current_file = ''
init()

def convert_test_file_to_csv(file_name):
    try:
        df = pd.read_csv(os.path.join(BASE_DIR_DATA, file_name), nrows=5)
    except Exception as e:
        df = pd.read_excel(os.path.join(BASE_DIR_DATA, file_name), nrows=5)
        df_tmp = pd.read_excel(os.path.join(BASE_DIR_DATA, file_name))
        name = file_name.split('.')[0] + '.csv'
        df_tmp.to_csv(os.path.join(BASE_DIR_DATA, name), index=False)
    return

# Create your views here.
def upload(request):
    if request.method == 'GET':
        return render(request, 'upload.html')
    else:
        name = request.FILES['train'].name
        handle_uploaded_file(request.FILES['train'], name)
        test_name = ""
        if request.FILES.get("test", None) is not None:
            test_name = request.FILES.get('test').name
            handle_uploaded_file(request.FILES['test'], test_name)
            convert_test_file_to_csv(test_name)
        try:
            df = pd.read_csv(os.path.join(BASE_DIR_DATA, name), nrows=5)
        except Exception as e:
            df = pd.read_excel(os.path.join(BASE_DIR_DATA, name), nrows=5)
            df_tmp = pd.read_excel(os.path.join(BASE_DIR_DATA, name))
            name = name.split('.')[0] + '.csv'
            print("Save new file to csv {}".format(name))
            df_tmp.to_csv(os.path.join(BASE_DIR_DATA, name), index=False)
            print("Done save file")
        cols = ["Column Name", "Type", "Ex1", "Ex2", "Ex3", "Ex4", "Ex5", "Is Target"]
        '''
        [{'col': col_name, 'type': type, 'values': [values]}]
        '''
        res = []
        for col in df.columns:
            col_type = None
            if is_numeric_dtype(df[col]):
                col_type = 'number'
            elif is_string_dtype(df[col]):
                col_type = 'string'
            d = {'col': col, 'type': col_type, 'values': df[col].tolist()}
            res.append(d)
        return render(request, "preview_data.html", {'records': res, 'file_name': name, 'cols': cols, "test_name": test_name})

def handle_uploaded_file(f, name):
    with open(os.path.join(BASE_DIR_DATA, name), 'wb') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

@lru_cache(maxsize=256)
def calculate_summary(file_name, target_name, n_unique):
    df = pd.read_csv(os.path.join(BASE_DIR_DATA, file_name))
    df.rename(columns={target_name: 'TARGET'}, inplace=True)
    df.to_csv(os.path.join(BASE_DIR_DATA, file_name), index=False)
    df.drop(['TARGET'], axis=1, inplace=True)
    df.replace([np.inf, -np.inf], np.nan, inplace=True)
    n_rows = df.shape[0]
    res_number_not_null = []
    res_number_is_null = []
    res_string_not_null = []
    res_string_is_null = []

    for col in df.columns:
        if df[col].dtype == np.float64 or df[col].dtype == np.int64:
            col_type = 'number'
            n_nans = df[col].isna().sum()
            n_uniques = df[col].nunique()
            d = {'col': col, 'type': col_type, 'n_nans': n_nans, 'n_uniques': n_uniques}

            if n_uniques <= n_unique:
                if n_nans == 0:
                    res_string_not_null.append(d)
                else:
                    res_string_is_null.append(d)
                continue
            if n_nans == 0:
                res_number_not_null.append(d)
            else:
                res_number_is_null.append(d)
        else:
            col_type = 'string'
            n_nans = df[col].isna().sum()
            n_uniques = df[col].nunique()
            d = {'col': col, 'type': col_type, 'n_nans': n_nans, 'n_uniques': n_uniques}
            if n_nans == 0:
                res_string_not_null.append(d)
            else:
                res_string_is_null.append(d)
    results = {'res_number_not_null': res_number_not_null, 'res_number_is_null': res_number_is_null,
               'res_string_not_null': res_string_not_null, 'res_string_is_null': res_string_is_null}
    cols = {'col_num_not_null': "|".join([item.get("col") for item in res_number_not_null]),
            'col_num_is_null': "|".join([item.get("col") for item in res_number_is_null]),
            'col_str_not_null': "|".join([item.get("col") for item in res_string_not_null]),
            'col_str_is_null': "|".join([item.get("col") for item in res_string_is_null]),
            }

    summary_totals = {'n_rows': n_rows, 'n_cols': len(df.columns),
                      'n_col_num_not_null': len(res_number_not_null), 'n_col_num_is_null': len(res_number_is_null),
                      'n_col_str_not_null': len(res_string_not_null), 'n_col_str_is_null': len(res_string_is_null)}
    return results, cols, summary_totals

def change_dict_to_df(dic):
    d_res = {}
    for prefix, vals in dic.items():
        if prefix in ['tr', 't']:
            for k, v in vals.items():
                if k not in ['tpr', 'fpr']:
                    d_res[str(prefix) + "_" + str(k)] = v
        else:
            d_res[prefix] = vals
    return d_res

def data_summary(request):
    global current_file
    if request.method == 'GET':
        file_name = request.GET['file_name']
        target_name = request.GET['target_name']
        n_unique = int(request.GET.get('n_unique', 50))
        current_file = os.path.join(BASE_DIR_DATA, file_name)
        # cols = ["Column Name", "Type", "Null Count", "Ex2", "Ex3", "Ex4", "Ex5"]
        '''
        [{'col': col_name, 'type': type, 'n_nans': n_nans}]
        '''
        results, cols, summary_totals = calculate_summary(file_name, target_name, n_unique)
        return render(request, "summary_data.html", {'results': results, 'file_name': file_name,
                                                     'cols': cols, 'summary_totals': summary_totals})
    else:
        fill_nan_cols = {}
        encoding_cols = {}
        embedding_cols = {}
        dim_cols = {}
        ignore_cols = []
        tmp_df = pd.read_csv(current_file, nrows=5)
        all_cols = list(tmp_df.columns)
        train_cols = [col for col in all_cols if col not in ignore_cols]
        for key, value in request.POST.items():
            print(key, value)
            t, col_name, val = parse_params(key, value)
            if t == 'emb':
                embedding_cols[col_name] = 0.5
            elif t == 'dim':
                if val is not None:
                    dim_cols[col_name] = float(val)
            elif t == 'nan':
                fill_nan_cols[col_name] = val
            elif t == 'encode':
                encoding_cols[col_name] = val
            elif t == 'ignore':
                ignore_cols.append(col_name)
        # print(ignore_cols)
        for ignore_col in ignore_cols:
            fill_nan_cols.pop(ignore_col, None)
            encoding_cols.pop(ignore_col, None)
            embedding_cols.pop(ignore_col, None)
            dim_cols.pop(ignore_col, None)
        for k, v in embedding_cols.items():
            embedding_cols[k] = dim_cols.get(k, v)
            if encoding_cols.get(k, None) == 'onehot':
                encoding_cols[k] = 'label'
        layers = request.POST.get("layers")
        layers = parse_layers(layers)
        dropouts = request.POST.get("dropouts")
        dropouts = [float(item) for item in dropouts.split("x")]
        if len(dropouts) < len(layers):
            for i in range(len(layers)-len(dropouts)):
                dropouts.append(0)
        elif len(dropouts) > len(layers):
            dropouts = dropouts[0: len(layers)]
        metrics = request.POST.getlist("metrics")
        loss_function = request.POST.get("loss_function")
        epochs = int(request.POST.get("epochs"))
        test_size = float(request.POST.get("test_size"))
        optimizer = request.POST.get("optimizer", "sgd")
        activation = request.POST.get("activation", "relu")

        suffix_file_name = str(int(time.time()))
        tmp_file_res = suffix_file_name + '.csv'
        # save config encode and embedding
        with open(os.path.join(BASE_DIR_CONFIG, suffix_file_name + '.txt'), 'w') as f:
            json.dump({'fill_nan': fill_nan_cols, 'encode': encoding_cols,
                       'embedding': embedding_cols, 'dim': dim_cols}, f)
        if len(embedding_cols) > 0:
            if len(set(list(embedding_cols.values())))==1:
                embed_config = "{}{}".format("emd", list(embedding_cols.values())[0])
            else:
                embed_config = "emd"
        elif len(encoding_cols) > 0:
            embed_config = "enc"
        else:
            embed_config = "raw"

        list_layer_trys = list(itertools.product(*layers))

        diff_cols = ['optimizer', 'activation', 'epochs', 't_percent_0',
                     't_percent_1', 'tr_percent_0', 'tr_percent_1']
        if test_size > 1:
            layer = list_layer_trys[0]
            model_config = {'n_folds': int(test_size), 'epochs': epochs, 'metrics': metrics,
                            'layers': layer, 'dropouts': dropouts, 'loss': loss_function,
                            'optimizer': optimizer, 'activation': activation}
            managerDataFlow = ManagerDataFlow(current_file, 'TARGET', ignore_cols)
            managerDataFlow.fill_nan(fill_nan_cols)
            managerDataFlow.encoding(encoding_cols)
            managerDataFlow.standard_scale(set(encoding_cols.keys()))
            managerDataFlow.embedding(embedding_cols)
            managerDataFlow.build_nn(model_config)
            fold_res = managerDataFlow.train_with_kfold(model_config)
            # current_res['layers'] = ",".join([str(item) for item in layer])
            # current_res['dropouts'] = "|".join([str(item) for item in dropouts])
            df_res = pd.DataFrame([change_dict_to_df(item) for item in fold_res])
            df_res.insert(0, 'layers', "x".join([str(item) for item in layer]))
            df_res.insert(1, 'dropouts', "x".join([str(item) for item in dropouts]))
            df_res.insert(2, 'optimizer', optimizer)
            df_res.insert(3, 'activation', activation)
            df_res.insert(4, 'epochs', epochs)
            df_res = round_df(df_res)
            df_res.to_csv(os.path.join(BASE_DIR_RESULT, tmp_file_res), index=False)
            df_res_sum = df_res.describe()
            df_res_sum = round_df(df_res_sum)
            # drop index count
            df_res_sum.drop('count', inplace=True)
            # drop columns
            df_res_sum.drop(['t_percent_0', 't_percent_1', 'tr_percent_0',
                             'tr_percent_1', 'fold', 'epochs'], axis=1, inplace=True)
            df_res_sum.rename({'25%': 'Q1', '50%': 'median', '75%': 'Q3'}, inplace=True)
            df_res_sum.reset_index().to_csv(
                os.path.join(BASE_DIR_RESULT, suffix_file_name + "_summary.csv"), index=False
            )
            res_path, res_sum_path = rename_file(suffix_file_name, os.path.join(BASE_DIR_RESULT, tmp_file_res),
                                                 os.path.join(BASE_DIR_RESULT, suffix_file_name + "_summary.csv"), embed_config)
            return render(request, 'result.html', {"result": read_result(res_path , diff_cols),
                                                   "result_summary": read_result(res_sum_path, []),
                                                   "auc_image": 'kfold.png',
                                                   "cols_use": train_cols})


        best_result = {}
        res = []
        for idx, layer in enumerate(list_layer_trys):
            print("Try {}/{}".format(idx, len(list_layer_trys)))
            model_config = {'layers': layer, 'dropouts': dropouts, 'loss': loss_function,
                            'optimizer': optimizer, 'activation': activation}
            managerDataFlow = ManagerDataFlow(current_file, 'TARGET', ignore_cols)
            managerDataFlow.fill_nan(fill_nan_cols)
            managerDataFlow.encoding(encoding_cols)
            managerDataFlow.standard_scale(set(encoding_cols.keys()))
            managerDataFlow.embedding(embedding_cols)
            managerDataFlow.build_nn(model_config)
            current_res = managerDataFlow.train({'test_size': test_size, 'epochs': epochs, 'metrics': metrics})
            current_res['layers'] = "x".join([str(item) for item in layer])
            current_res['dropouts'] = "x".join([str(item) for item in dropouts])
            current_res['optimizer'] = optimizer
            current_res['activation'] = activation
            current_res['epochs'] = epochs
            res.append(current_res)
            try:
                current_auc = current_res.get("t", {}).get('auc')
                if current_auc > best_result.get("t", {}).get('auc', 0):
                    best_result = current_res
            except:
                current_acc = current_res.get("t", {}).get('auc')
                if current_acc > best_result.get("t", {}).get('auc', 0):
                    best_result = current_res
            print("Current Result: {}".format(current_res))
            print("Best Result: {}".format(best_result))
            if idx == 0:
                df_res = pd.DataFrame([change_dict_to_df(current_res)])
                df_res.to_csv(
                    os.path.join(BASE_DIR_RESULT, tmp_file_res),
                    index=False)
            else:
                df_res = pd.DataFrame([change_dict_to_df(current_res)])
                df_res.to_csv(
                    os.path.join(BASE_DIR_RESULT, tmp_file_res),
                    index=False, mode='a', header=False)
        res_path, _ = rename_file(suffix_file_name, os.path.join(BASE_DIR_RESULT, tmp_file_res), embed_config=embed_config)
        return render(request, 'result.html', {"result": read_result(res_path, diff_cols),
                                               "cols_use": train_cols})

def rename_file(suffix_name, result_file_name, result_summary_file_name=None, embed_config=None):
    if result_summary_file_name is None:
        # not kfold, find row have max value of auc and acc
        df_res = pd.read_csv(result_file_name)
        max_auc = df_res['t_auc'].max()
        idx_max_auc = df_res['t_auc'].argmax()
        acc = df_res.iloc[idx_max_auc]['t_acc']
        layers_cf = df_res.iloc[idx_max_auc]['layers']
        droputs_cf = df_res.iloc[idx_max_auc]['dropouts']
        optimizer_cf = df_res.iloc[idx_max_auc]['optimizer']
        activation_cf = df_res.iloc[idx_max_auc]['activation']
        res_file_change = "{}_{}_{}_{}_{}_{}_{}.{}".format(acc, max_auc, optimizer_cf, activation_cf, droputs_cf, embed_config, suffix_name, layers_cf)
        res_path_change = os.path.join(BASE_DIR_RESULT, res_file_change)
        os.rename(result_file_name, res_path_change)
        return res_path_change, None
    else:
        # kfold, calculate mean of auc.
        df_res = pd.read_csv(result_file_name)
        df_res_summary = pd.read_csv(result_summary_file_name)
        mean_auc = df_res_summary.iloc[0]['t_auc']
        mean_acc = df_res_summary.iloc[0]['t_acc']
        layers_cf = df_res.iloc[0]['layers']
        droputs_cf = df_res.iloc[0]['dropouts']
        optimizer_cf = df_res.iloc[0]['optimizer']
        activation_cf = df_res.iloc[0]['activation']
        res_file_change = "{}_{}_{}_{}_{}_{}".format(mean_acc, mean_auc, optimizer_cf, activation_cf, droputs_cf, embed_config)
        res_path_change = os.path.join(BASE_DIR_RESULT, "{}_{}.{}".format(res_file_change, suffix_name, layers_cf))
        res_summary_path_change = os.path.join(BASE_DIR_RESULT, "{}_{}_summary.{}".format(res_file_change, suffix_name, layers_cf))
        os.rename(result_file_name, res_path_change)
        os.rename(result_summary_file_name, os.path.join(BASE_DIR_RESULT, res_summary_path_change))
        return res_path_change, res_summary_path_change




def read_result(path, diff_cols):
    # diff_cols = ['optimizer', 'activation', 'epochs', 'test_percent_0',
    #              'test_percent_1', 'train_percent_0', 'train_percent_1']
    df_res = pd.read_csv(path)
    if len(diff_cols) != 0:
        df_res = df_res[df_res.columns.difference(diff_cols)]
        first_column = 'fold'
        if first_column in df_res.columns:
            df_res = df_res.reindex(columns=([first_column] + list([a for a in df_res.columns if a != first_column])))
    df_res = round_df(df_res)
    return {'cols': list(df_res.columns), 'records': df_res.values.tolist(), 'file_name': path}

def parse_layers(layers):
    res = []
    for layer in layers.split("x"):
        layer_split = layer.split("-")
        if len(layer_split) == 1:
            res.append([int(layer_split[0])])
        elif len(layer_split) == 2:
            res.append([num for num in range(int(layer_split[0]), int(layer_split[1]))])
    return res

def parse_params(key, value):
    if key[0:3] == 'tb2' and key != 'tb2_choose_all' and key != 'tb2_emball' and key != 'tb2_emb_choose_all':
        if key[4:8] == 'emb_':
            col_name = key[8:]
            return 'emb', col_name, value
        elif key[4:18] == 'dimension_emb_':
            col_name = key[18:]
            try:
                dim = float(value)
            except:
                dim = None
            return 'dim', col_name, dim
        elif key[4:10] == 'ignore':
            col_name = key[11:]
            return 'ignore', col_name, value
        else:
            col_name = key[4:]
            return 'encode', col_name, value.split("_")[0]
    elif key[0:3] == 'tb3':
        col_name = key[8:]
        return 'nan', col_name, value
    elif key[0:3] == 'tb4' and key != 'tb4_choose_all' and key != 'tb4_emball' and key != 'tb4_emb_choose_all':
        if key[4:8] == 'emb_':
            col_name = key[8:]
            return 'emb', col_name, value
        elif key[4:18] == 'dimension_emb_':
            col_name = key[18:]
            try:
                dim = float(value)
            except:
                dim = None
            return 'dim', col_name, dim
        elif key[4:8] == 'nan_':
            col_name = key[8:]
            return 'nan', col_name, value
        else:
            col_name = key[4:]
            return 'encode', col_name, value.split("_")[0]
    return None, None, None

def make_crosspair(request):
    return render(request, 'test.html')

def wd_summary(request):
    global current_file
    if request.method == 'GET':
        file_name = request.GET['file_name']
        target_name = request.GET['target_name']
        n_unique = int(request.GET.get('n_unique', 50))
        current_file = os.path.join(BASE_DIR_DATA, file_name)
        # cols = ["Column Name", "Type", "Null Count", "Ex2", "Ex3", "Ex4", "Ex5"]
        '''
        [{'col': col_name, 'type': type, 'n_nans': n_nans}]
        '''
        results, cols, summary_totals = calculate_summary(file_name, target_name, n_unique)
        return render(request, "summary_data.html", {'results': results, 'file_name': file_name,
                                                     'cols': cols, 'summary_totals': summary_totals})
    else:
        pass
