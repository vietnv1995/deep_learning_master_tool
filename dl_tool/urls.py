from django.urls import path
from . import views

urlpatterns = [
    path('', views.upload, name='index'),
    path('data-summary', views.data_summary, name='data_summary'),
    path('cross_col', views.make_crosspair, name='cross_col'),
    path('wd-summary', views.wd_summary, name='wd_summary')
]