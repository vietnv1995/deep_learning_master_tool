import keras
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from keras.layers import Dense, Dropout, Input, Reshape, Concatenate
from keras.layers.embeddings import Embedding
from sklearn.model_selection import train_test_split
from keras.models import Model
import numpy as np
from sklearn import metrics
from sklearn.preprocessing import StandardScaler
import time
from dl_tool.core.encoding_method import MethodEncoder
from dl_tool.core.encoding_method import MethodCode
from dl_tool.core.fill_nan_method import FillNanMethod
from dl_tool.core.fill_nan_method import FillNanCode
from dl_tool.core.const import ROUND
from sklearn.model_selection import KFold
from matplotlib import pyplot as plt

class ManagerDataFlow:
    def __init__(self, file_name, col_target, col_ignore):
        self.file_name = file_name
        self.col_target = col_target
        self.col_ignore = col_ignore
        self.read_data()
        self.inputs = []
        self.embeds = []
        self.col_embeds = []

    def read_data(self):
        self.df_raw = pd.read_csv(self.file_name)
        print("Drop {}".format(self.col_ignore))
        print(self.df_raw.head())
        self.df_raw = self.df_raw.drop(self.col_ignore, axis=1)
        self.df = self.df_raw[self.df_raw.columns.difference([self.col_target])]
        self.df.replace([np.inf, -np.inf], np.nan, inplace=True)
        self.df_target = self.df_raw[[self.col_target]]
        if self.df_target[self.col_target].nunique() == 2:
            self.n_target = 1
        else:
            self.n_target = self.df_target.nunique()

    def fill_nan(self, dic):
        fill_nan_obj = FillNanMethod(self.df)
        for col, method in dic.items():
            print("Fill nan column {} by {}".format(col, method))
            if method == 'mean':
                fill_nan_obj.fillnan(FillNanCode.FillMean, col)
            elif method == 'mode':
                fill_nan_obj.fillnan(FillNanCode.FillMode, col)
            elif method == 'zero':
                fill_nan_obj.fillnan(FillNanCode.FillZero, col)
        self.df = fill_nan_obj.get_result()

    def encoding(self, dic):
        encoder = MethodEncoder(self.df)
        for col, method in dic.items():
            print("Encoding column {} by {}".format(col, method))
            if method == 'label':
                # print(self.df[col].unique())
                encoder.encode(MethodCode.MethodLabel, col)
            elif method == 'onehot':
                encoder.encode(MethodCode.MethodOnehot, col)
        self.df = encoder.get_result()

    def encoding_target(self):
        encoder = MethodEncoder(self.df_target)
        if self.n_target == 1:
            encoder.encode(MethodCode.MethodLabel, self.col_target)
        else:
            encoder.encode(MethodCode.MethodOnehot, self.col_target)
        self.df_target = encoder.get_result()

    def preproc(self, df):
        input_list_train = []
        for c in self.col_embeds:
            input_list_train.append(df[c].values)
            """
            raw_vals = np.unique(X_train[c])
            val_map = {}
            for i in range(len(raw_vals)):
                val_map[raw_vals[i]] = i       
            input_list_train.append(X_train[c].map(val_map).values)
            """
        # the rest of the columns
        if len(self.col_embeds) < df.shape[1]:
            input_list_train.append(df[df.columns.difference(self.col_embeds)].values)
        return input_list_train


    def embedding(self, dic):
        '''
        :param dic:
        col_name: 0.5 or 30
        :return:
        '''
        if len(dic) == 0:
            return
        for col, dim in dic.items():
            self.col_embeds.append(col)
            no_of_unique_cat = self.df[col].nunique()
            if dim <= 1:
                dim = int(no_of_unique_cat*dim)
            else:
                dim = int(dim)
            input_cat = Input(shape=(1,))
            embed = Embedding(no_of_unique_cat, dim, input_length=1)(input_cat)
            embed = Reshape(target_shape=(dim,))(embed)
            self.inputs.append(input_cat)
            self.embeds.append(embed)
        n_other_cols = len(self.df.columns) - len(dic)
        if n_other_cols > 0:
            input_other = Input(shape=(n_other_cols,))
            self.inputs.append(input_other)
            embed_other = Dense(n_other_cols)(input_other)
            self.embeds.append(embed_other)

    def build_nn(self, dic):
        '''
        :param dic:
        layers: [100, 200, 300]
        dropouts: [0,2; 0,3; 0,5]
        loss: mean_squared_error
        :return:
        '''
        if len(self.embeds) == 0:
            self.model = keras.Sequential()
            for idx, n_neuron in enumerate(dic.get("layers")):
                if idx == 0:
                    self.model.add(Dense(n_neuron, activation=dic.get("activation"), input_shape=(self.df.shape[1],)))
                else:
                    self.model.add(Dense(n_neuron, activation=dic.get("activation")))
                dropout = dic.get("dropouts")[idx]
                print("Dropout ", dropout)
                if dropout != 0:
                    self.model.add(Dropout(dropout))
            self.model.add(Dense(self.n_target, activation='sigmoid'))
            self.model.compile(optimizer=dic.get("optimizer"), loss=dic.get("loss"), metrics=['accuracy'])
        else:
            x = Concatenate()(self.embeds)
            for idx, n_neuron in enumerate(dic.get("layers")):
                x = Dense(n_neuron, activation=dic.get("activation"))(x)
                dropout = dic.get("dropouts")[idx]
                if dropout != 0:
                    x = Dropout(dropout)(x)
            output = Dense(self.n_target, activation='sigmoid')(x)
            self.model = Model(self.inputs, output)
            self.model.compile(optimizer=dic.get("optimizer"), loss=dic.get("loss"), metrics=['accuracy'])

    def standard_scale(self, cols):
        for col in self.df.columns:
            if col not in cols:
                print("Scale column {}".format(col))
                stdSc = StandardScaler()
                self.df[col] = stdSc.fit_transform(self.df[col].values.reshape(-1, 1))[:, 0]


    def train(self, dic):
        '''
        :param dic:
        test_size: 0.3
        epochs: 1000
        metric: ['acc', 'auc', 'confus']
        :return:
        '''
        if len(self.embeds) == 0:
            X, y = self.df.values, self.df_target.values
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=dic['test_size'], random_state=42)
            print(self.model.summary())

            start = time.time()
            history = self.model.fit(X_train, y_train, epochs=dic['epochs'])
            end = time.time()
            res = self.evaluate(history, self.model, X_train, X_test, y_train, y_test, dic)
            res['train_time'] = round(end - start, ROUND)
        else:
            X, y = self.df.values, self.df_target.values
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=dic['test_size'], random_state=42)
            df_train = pd.DataFrame(X_train, columns=self.df.columns)
            df_test = pd.DataFrame(X_test, columns=self.df.columns)
            input_list_train = self.preproc(df_train)
            input_list_test = self.preproc(df_test)

            start = time.time()
            history = self.model.fit(input_list_train, y_train, epochs=dic['epochs'])
            end = time.time()
            res = self.evaluate(history, self.model, input_list_train, input_list_test, y_train, y_test, dic)
            res['train_time'] = round(end - start, ROUND)
        keras.backend.clear_session()
        return res

    def train_with_kfold(self, dic):
        '''

        :param dic:
        n_folds
        :return:
        '''
        all_res = []
        cv = KFold(n_splits=dic.get('n_folds'), random_state=42, shuffle=True)
        X, y = self.df.values, self.df_target.values
        for fold, (train_index, test_index) in enumerate(cv.split(X)):
            current_model = keras.models.clone_model(self.model)
            current_model.set_weights(self.model.get_weights())
            current_model.compile(optimizer=dic.get("optimizer"), loss=dic.get("loss"), metrics=['accuracy'])
            if len(self.embeds) == 0:
                X_train, X_test, y_train, y_test = X[train_index], X[test_index], y[train_index], y[test_index]
                print(current_model.summary())

                start = time.time()
                current_history = current_model.fit(X_train, y_train, epochs=dic['epochs'])
                end = time.time()
                res = self.evaluate(current_history, current_model, X_train, X_test, y_train, y_test, dic)
                res['train_time'] = round(end - start, ROUND)
            else:
                X_train, X_test, y_train, y_test = X[train_index], X[test_index], y[train_index], y[test_index]
                df_train = pd.DataFrame(X_train, columns=self.df.columns)
                df_test = pd.DataFrame(X_test, columns=self.df.columns)
                input_list_train = self.preproc(df_train)
                input_list_test = self.preproc(df_test)

                start = time.time()
                history = current_model.fit(input_list_train, y_train, epochs=dic['epochs'])
                end = time.time()
                res = self.evaluate(history, current_model, input_list_train, input_list_test, y_train, y_test, dic)
                res['train_time'] = round(end - start, ROUND)
            print("Train at fold {} have res {}".format(fold+1, res))
            res['fold'] = fold+1
            all_res.append(res)
        keras.backend.clear_session()
        self.export_auc(all_res)
        return all_res


    def export_auc(self, all_res):
        plt.figure()
        for res in all_res:
            item = res['t']
            fold = res['fold']
            tpr, fpr = item.pop('tpr'), item.pop('fpr')
            auc = item['auc']
            plt.plot(fpr, tpr, label="Fold {}, AUC = {}".format(fold, auc))
        plt.legend(loc=0)
        plt.savefig("dl_tool/static/kfold.png")

    def evaluate(self, history, model, X_train, X_test, y_train, y_test, dic):
        res_train = {}
        res_test = {}
        y_pred = model.predict(X_test)
        y_classes = (y_pred >= 0.5).astype(int)

        y_train_pred = model.predict(X_train)
        y_train_classes = (y_train_pred >= 0.5).astype(int)
        for metric in dic.get("metrics"):
            if metric == 'auc':
                fpr, tpr, thresholds = metrics.roc_curve(y_test, y_pred)
                res_test['auc'] = round(metrics.auc(fpr, tpr), ROUND)
                res_test['fpr'] = fpr
                res_test['tpr'] = tpr

                fpr, tpr, thresholds = metrics.roc_curve(y_train, y_train_pred)
                res_train['auc'] = round(metrics.auc(fpr, tpr), ROUND)
            elif metric == 'acc':
                res_test['acc'] = round(metrics.accuracy_score(y_test, y_classes), ROUND)
                res_train['acc'] = round(metrics.accuracy_score(y_train, y_train_classes), )
            elif metric == 'confus':
                print(metrics.confusion_matrix(y_test, y_classes))
                res_test['confus'] = str([list(item) for item in metrics.confusion_matrix(y_test, y_classes)])
                res_train['confus'] = str([list(item) for item in metrics.confusion_matrix(y_train, y_train_classes)])
            elif metric == 'precision':
                res_test['precision'] = round(metrics.precision_score(y_test, y_classes), ROUND)
                res_train['precision'] = round(metrics.precision_score(y_train, y_train_classes), ROUND)
            elif metric == 'recall':
                res_test['recall'] = round(metrics.recall_score(y_test, y_classes), ROUND)
                res_train['recall'] = round(metrics.recall_score(y_train, y_train_classes), ROUND)
            elif metric == 'f1_score':
                res_test['f1'] = round(metrics.f1_score(y_test, y_classes), ROUND)
                res_train['f1'] = round(metrics.f1_score(y_train, y_train_classes), ROUND)
            elif metric == 'specificity':
                tn, fp, fn, tp = metrics.confusion_matrix(y_test, y_classes).ravel()
                res_test['specificity'] = round(tn / (tn+fp), ROUND)
                res_test['percent_0'] = round((fp + tn) / (tn + fp + fn + tp)*100, ROUND)
                res_test['percent_1'] = 100-res_test['percent_0']

                tn, fp, fn, tp = metrics.confusion_matrix(y_test, y_classes).ravel()
                res_train['specificity'] = round(tn / (tn+fp), ROUND)
                res_train['percent_0'] = round((fp+tn)/(tn+fp+fn+tp)*100, ROUND)
                res_train['percent_1'] = 100-res_train['percent_0']
            elif metric == 'logloss':
                if history != None:
                    res_train['logloss'] = round(history.history['loss'][-1], ROUND)
        res = {'tr': res_train, 't': res_test}
        return res




