

class FillNanCode(object):
    FillMean = 1
    FillMode = 2
    FillZero = 3

class FillNanMethod(object):
    def __init__(self, df):
        self.df = df

    def fillnan(self, method, col_name):
        if method == FillNanCode.FillMean:
            self.df[col_name].fillna(self.df[col_name].mean(), inplace=True)
        if method == FillNanCode.FillMode:
            self.df[col_name].fillna(self.df[col_name].mode()[0], inplace=True)
        if method == FillNanCode.FillZero:
            self.df[col_name].fillna(0, inplace=True)

    def get_result(self):
        return self.df