from dl_tool.core.const import ROUND
import numpy as np

def round_df(df):
    for col in df.columns:
        if df[col].dtypes == np.float64:
            df[col] = [round(item, ROUND) for item in df[col].tolist()]
    return df